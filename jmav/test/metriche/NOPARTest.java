package metriche;

import jmav.component.CalcolatoreMetriche;
import jmav.object.Metrica;
import jmav.object.Posizione;
import junit.framework.TestCase;

import org.junit.Test;

public class NOPARTest extends TestCase {
	public static String MethodWith0Par = "public class A { void method1(){} }";
	public static String MethodWith1Par = "public class A { void method1(Object a){} }";
	public static String MethodWithNPar = "public class A { void method1(Object a, Object b){} }";
	public static String MethodWithVaradicPar = "public class A { void method1(Object... values){} }";
	public static String MethodWithVaradicParWith1Par = "public class A { void method1(Object a, Object... values){} }";
	public static String MethodWithVaradicParWith2Par = "public class A { void method1(Object a, Object b, Object... values){} }";
	
	// CASO 1: Metodo con 0 parametri
	@Test
	public void testNOPAR_1(Object... values) throws Exception {
		Posizione posizione = UtilTest.getMetodo(MethodWith0Par);
		assertNotNull(posizione);
		System.out.println("\n\nposizione: "+posizione.getNode());
		
		CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
		Double numberOfParameters = calcolatoreMetriche.calcolaMatrica(Metrica.NOPAR, posizione);
		System.out.println("numberOfParameters: "+numberOfParameters);
		assertEquals(numberOfParameters, 0.0);
	}
	
	// CASO 2: Metodo con 1 parametro
	@Test
	public void testNOPAR_2() throws Exception {
		Posizione posizione = UtilTest.getMetodo(MethodWith1Par);
		assertNotNull(posizione);
		System.out.println("\n\nposizione: "+posizione.getNode());
		
		CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
		Double numberOfParameters = calcolatoreMetriche.calcolaMatrica(Metrica.NOPAR, posizione);
		System.out.println("numberOfParameters: "+numberOfParameters);
		assertEquals(numberOfParameters, 1.0);
	}
	
	// CASO 3: Metodo con N parametri
	@Test
	public void testNOPAR_3() throws Exception {
		Posizione posizione = UtilTest.getMetodo(MethodWithNPar);
		assertNotNull(posizione);
		System.out.println("\n\nposizione: "+posizione.getNode());
		
		CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
		Double numberOfParameters = calcolatoreMetriche.calcolaMatrica(Metrica.NOPAR, posizione);
		System.out.println("numberOfParameters: "+numberOfParameters);
		assertEquals(numberOfParameters, 2.0);
	}
	
	// CASO 4: Metodo con 1 parametro varadic
	@Test
	public void testNOPAR_4() throws Exception {
		Posizione posizione = UtilTest.getMetodo(MethodWithVaradicPar);
		assertNotNull(posizione);
		System.out.println("\n\nposizione: "+posizione.getNode());
		
		CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
		Double numberOfParameters = calcolatoreMetriche.calcolaMatrica(Metrica.NOPAR, posizione);
		System.out.println("numberOfParameters: "+numberOfParameters);
		assertEquals(numberOfParameters, 1.0);
	}
	
	// CASO 5: Metodo con 1 parametro varadic con 1 parametro normale
	@Test
	public void testNOPAR_5() throws Exception {
		Posizione posizione = UtilTest.getMetodo(MethodWithVaradicParWith1Par);
		assertNotNull(posizione);
		System.out.println("\n\nposizione: "+posizione.getNode());
		
		CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
		Double numberOfParameters = calcolatoreMetriche.calcolaMatrica(Metrica.NOPAR, posizione);
		System.out.println("numberOfParameters: "+numberOfParameters);
		assertEquals(numberOfParameters, 2.0);
	}
	
	// CASO 5: Metodo con 1 parametro varadic con 1 parametro normale
	@Test
	public void testNOPAR_6() throws Exception {
		Posizione posizione = UtilTest.getMetodo(MethodWithVaradicParWith2Par);
		assertNotNull(posizione);
		System.out.println("\n\nposizione: "+posizione.getNode());
		
		CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
		Double linesOfCode = calcolatoreMetriche.calcolaMatrica(Metrica.NOPAR, posizione);
		System.out.println("linesOfCode: "+linesOfCode);
		assertEquals(linesOfCode, 3.0);
	}
}