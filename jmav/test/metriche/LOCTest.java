package metriche;

import jmav.component.CalcolatoreMetriche;
import jmav.object.Metrica;
import jmav.object.Posizione;
import junit.framework.TestCase;

import org.junit.Test;

public class LOCTest extends TestCase {
	
	public static String ClassWith2LinesOfCode = "public class A {}";
	public static String MethodWith2LinesOfCode = "public class A { void method1(){} }";
	
	public static String ClassWithNLinesOfCodeWithoutComment = "public class A { private int n; void method1(){}; }";
	public static String ClassWithNLinesOfCodeSingleLineComment = "public class A { private int n1; //private int n2; void method1(){}; }";
	public static String ClassWithNLinesOfCodeMultiLineComment = "public class A { private int n1; /*private int n2; void method1(){};*/ }";
	
	public static String MethodWithNLinesOfCodeWithoutComment = "public class A { void method1(){ int n1 = 5; } }";
	public static String MethodWithNLinesOfCodeSingleLineComment = "public class A { void method1(){ int n1 = 5; //int n2 = 10; } }";
	public static String MethodWithNLinesOfCodeMultiLineComment = "public class A { void method1(){ int n1 = 5; /*int n2 = 10; int n3 = 25; */} }";
	
	public static String MethodWithAbstract = "public abstract class A { void abstract method1(); }";
	
	// CASO 1: Classe con 2 linee di codice (solo la dichiarazione)
	@Test
	public void testLOC_1() throws Exception {
		Posizione posizione = UtilTest.getClasse(ClassWith2LinesOfCode);
		assertNotNull(posizione);
		System.out.println("\n\nposizione: "+posizione.getNode());
		
		CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
		Double linesOfCode = calcolatoreMetriche.calcolaMatrica(Metrica.LOC, posizione);
		System.out.println("linesOfCode: "+linesOfCode);
		assertEquals(2.0, linesOfCode);
	}
	
	// CASO 2: Metodo con 2 linee di codice (solo la dichiarazione)
	@Test
	public void testLOC_2() throws Exception {
		Posizione posizione = UtilTest.getMetodo(MethodWith2LinesOfCode);
		assertNotNull(posizione);
		System.out.println("\n\nposizione: "+posizione.getNode());
		
		CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
		Double linesOfCode = calcolatoreMetriche.calcolaMatrica(Metrica.LOC, posizione);
		System.out.println("linesOfCode: "+linesOfCode);
		assertEquals(2.0, linesOfCode);
	}
	
	// CASO 3: Classe con N linee di codice senza commenti
	@Test
	public void testLOC_3() throws Exception {
		Posizione posizione = UtilTest.getClasse(ClassWithNLinesOfCodeWithoutComment);
		assertNotNull(posizione);
		System.out.println("\n\nposizione: "+posizione.getNode());
		
		CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
		Double linesOfCode = calcolatoreMetriche.calcolaMatrica(Metrica.LOC, posizione);
		System.out.println("linesOfCode: "+linesOfCode);
		assertEquals(5.0, linesOfCode);
	}

	// CASO 4: Classe con N linee di codice con commento singola linea
	@Test
	public void testLOC_4() throws Exception {
		Posizione posizione = UtilTest.getClasse(ClassWithNLinesOfCodeSingleLineComment);
		assertNotNull(posizione);
		System.out.println("\n\nposizione: "+posizione.getNode());
		
		CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
		Double linesOfCode = calcolatoreMetriche.calcolaMatrica(Metrica.LOC, posizione);
		System.out.println("linesOfCode: "+linesOfCode);
		assertEquals(3.0, linesOfCode);
	}
	
	// CASO 5: Classe con N linee di codice con commento multi linea
	@Test
	public void testLOC_5() throws Exception {
		Posizione posizione = UtilTest.getClasse(ClassWithNLinesOfCodeMultiLineComment);
		assertNotNull(posizione);
		System.out.println("\n\nposizione: "+posizione.getNode());
		
		CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
		Double linesOfCode = calcolatoreMetriche.calcolaMatrica(Metrica.LOC, posizione);
		System.out.println("linesOfCode: "+linesOfCode);
		assertEquals(3.0, linesOfCode);
	}
	
	// CASO 6: Metodo con N linee di codice senza commenti
	@Test
	public void testLOC_6() throws Exception {
		Posizione posizione = UtilTest.getMetodo(MethodWithNLinesOfCodeWithoutComment);
		assertNotNull(posizione);
		System.out.println("\n\nposizione: "+posizione.getNode());
		
		CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
		Double linesOfCode = calcolatoreMetriche.calcolaMatrica(Metrica.LOC, posizione);
		System.out.println("linesOfCode: "+linesOfCode);
		assertEquals(3.0, linesOfCode);
	}

	// CASO 7: Metodo con N linee di codice con commento singola linea
	@Test
	public void testLOC_7() throws Exception {
		Posizione posizione = UtilTest.getMetodo(MethodWithNLinesOfCodeSingleLineComment);
		assertNotNull(posizione);
		System.out.println("\n\nposizione: "+posizione.getNode());
		
		CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
		Double linesOfCode = calcolatoreMetriche.calcolaMatrica(Metrica.LOC, posizione);
		System.out.println("linesOfCode: "+linesOfCode);
		assertEquals(3.0, linesOfCode);
	}
	
	// CASO 8: Metodo con N linee di codice con commento multi linea
	@Test
	public void testLOC_8() throws Exception {
		Posizione posizione = UtilTest.getMetodo(MethodWithNLinesOfCodeMultiLineComment);
		assertNotNull(posizione);
		System.out.println("\n\nposizione: "+posizione.getNode());
		
		CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
		Double linesOfCode = calcolatoreMetriche.calcolaMatrica(Metrica.LOC, posizione);
		System.out.println("linesOfCode: "+linesOfCode);
		assertEquals(3.0, linesOfCode);
	}
	
	// CASO 9: Metodo abstract
	@Test
	public void testLOC_9() throws Exception {
		Posizione posizione = UtilTest.getMetodo(MethodWithAbstract);
		assertNotNull(posizione);
		System.out.println("\n\nposizione: "+posizione.getNode());
		
		CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
		Double linesOfCode = calcolatoreMetriche.calcolaMatrica(Metrica.LOC, posizione);
		System.out.println("linesOfCode: "+linesOfCode);
		assertEquals(1.0, linesOfCode);
	}
}