package metriche;

import jmav.object.Granularita;
import jmav.object.JavaProject;
import jmav.object.Posizione;
import jmav.visitor.ClassVisitor;
import jmav.visitor.MethodVisitor;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;

public class UtilTest {
	public static Posizione getClasse(String text) {
		CompilationUnit compilationUnit = JavaProject.parse(text.toCharArray());
		ClassVisitor visitor = new ClassVisitor();
		compilationUnit.accept(visitor);
		
		ASTNode node = null;
		
		for(TypeDeclaration td : visitor.getClasses()) {
			node = td;
		}
		
		return new Posizione(node, Granularita.CLASS);
	}
	
	public static Posizione getMetodo(String text) {
		CompilationUnit compilationUnit = JavaProject.parse(text.toCharArray());
		MethodVisitor visitor = new MethodVisitor();
		compilationUnit.accept(visitor);
		
		ASTNode node = null;
		
		for(MethodDeclaration md : visitor.getMethods()) {
			node = md;
		}
		
		return new Posizione(node, Granularita.METHOD);
	}
}
