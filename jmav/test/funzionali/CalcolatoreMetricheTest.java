package funzionali;

import jmav.component.CalcolatoreMetriche;
import jmav.exception.ParameterNotNullException;
import jmav.object.Metrica;
import junit.framework.TestCase;
import metriche.LOCTest;
import metriche.UtilTest;

import org.junit.Test;

public class CalcolatoreMetricheTest extends TestCase {
	@Test
	public void testCalcoloMetricaMetricaNull() throws Exception {
		try
		{
			CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
			calcolatoreMetriche.calcolaMatrica(null, UtilTest.getClasse(LOCTest.ClassWith2LinesOfCode));
			assertTrue(false);
		} catch(ParameterNotNullException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testCalcoloMetricaMetricaNotNullAndPositionNull() throws Exception {
		try
		{
			CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
			calcolatoreMetriche.calcolaMatrica(Metrica.LOC, null);
			assertTrue(false);
		} catch(ParameterNotNullException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testCalcoloMetricaMetricaNotNullAndPositionNotNull() throws Exception {
		try
		{
			CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
			calcolatoreMetriche.calcolaMatrica(Metrica.LOC, UtilTest.getClasse(LOCTest.ClassWith2LinesOfCode));
			assertTrue(true);
		} catch(ParameterNotNullException e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testInterrompiCalcoloMetriche() throws Exception {
		// TODO
	}
}
