package funzionali;

import jmav.component.CalcolatoreMetriche;
import jmav.component.GestoreAnalisi;
import jmav.component.GestoreImpostazioni;
import jmav.exception.AnalysisAlreadyStartedException;
import jmav.object.JavaProject;
import junit.framework.TestCase;

import org.junit.Test;

public class GestoreAnalisiTest extends TestCase {
	@Test
	public void testAvviaAnalisi() throws Exception {
		try 
		{
			String projectpath = "../test-artefatto";
			JavaProject project = new JavaProject(projectpath);
			
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.aggiungiSmell(GestoreImpostazioni.LargeClass);
			gestoreImpostazioni.modificaSoglia(GestoreImpostazioni.LargeClass, 1, 50);
			
			CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
			
			GestoreAnalisi gestoreAnalisi = new GestoreAnalisi(gestoreImpostazioni, calcolatoreMetriche);
			gestoreAnalisi.avviaAnalisi(project);
			
			System.out.println("LC GestoreAnalisi.listaRisultati: "+gestoreAnalisi.listaRisultati());
			assertTrue(true);
		} catch(Exception e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testAvviaAnalisiDoppia() throws Exception {
		try 
		{
			String projectpath = "../test-artefatto";
			JavaProject project = new JavaProject(projectpath);
			
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.aggiungiSmell(GestoreImpostazioni.LargeClass);
			gestoreImpostazioni.modificaSoglia(GestoreImpostazioni.LargeClass, 1, 50);
			
			CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
			
			GestoreAnalisi gestoreAnalisi = new GestoreAnalisi(gestoreImpostazioni, calcolatoreMetriche);
			gestoreAnalisi.avviaAnalisi(project);
			gestoreAnalisi.avviaAnalisi(project);
			
			System.out.println("LC GestoreAnalisi.listaRisultati: "+gestoreAnalisi.listaRisultati());
			assertTrue(false);
		} catch(AnalysisAlreadyStartedException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testInterrompiAnalisi() throws Exception {
		// TODO
	}
	
	@Test
	public void controllaAnalisiCorrente() throws Exception {
		try 
		{
			String projectpath = "../test-artefatto";
			JavaProject project = new JavaProject(projectpath);
			
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.aggiungiSmell(GestoreImpostazioni.LargeClass);
			gestoreImpostazioni.modificaSoglia(GestoreImpostazioni.LargeClass, 1, 50);
			
			CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
			
			GestoreAnalisi gestoreAnalisi = new GestoreAnalisi(gestoreImpostazioni, calcolatoreMetriche);
			assertEquals(false, gestoreAnalisi.controllaAnalisiCorrente());
			gestoreAnalisi.avviaAnalisi(project);
			assertEquals(true, gestoreAnalisi.controllaAnalisiCorrente());
			
			System.out.println("LC GestoreAnalisi.listaRisultati: "+gestoreAnalisi.listaRisultati());
			assertTrue(true);
		} catch(Exception e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testRisultatiAnalisi() throws Exception {
		try 
		{
			String projectpath = "../test-artefatto";
			JavaProject project = new JavaProject(projectpath);
			
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.aggiungiSmell(GestoreImpostazioni.LargeClass);
			gestoreImpostazioni.modificaSoglia(GestoreImpostazioni.LargeClass, 1, 50);
			
			CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
			
			GestoreAnalisi gestoreAnalisi = new GestoreAnalisi(gestoreImpostazioni, calcolatoreMetriche);
			gestoreAnalisi.avviaAnalisi(project);
			
			System.out.println("LC GestoreAnalisi.listaRisultati: "+gestoreAnalisi.listaRisultati());
			assertTrue(true);
			assertEquals(2, gestoreAnalisi.listaRisultati().size());
			assertEquals(true, gestoreAnalisi.listaRisultati().get(0).isYes());
			assertEquals(false, gestoreAnalisi.listaRisultati().get(1).isYes());
		} catch(Exception e) {
			assertTrue(false);
		}
	}
}
