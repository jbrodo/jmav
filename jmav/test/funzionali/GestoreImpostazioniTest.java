package funzionali;

import jmav.component.GestoreImpostazioni;
import jmav.exception.ParameterNotNullException;
import jmav.exception.ValueNotFoundException;
import junit.framework.TestCase;

import org.junit.Test;

public class GestoreImpostazioniTest extends TestCase {
	@Test
	public void testAggiungiIdSmellNull() throws Exception {
		try
		{
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.aggiungiSmell(null);
			assertTrue(false);
		} catch(ParameterNotNullException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testAggiungiIdSmellNonValido() throws Exception {
		try
		{
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.aggiungiSmell(4);
			assertTrue(false);
		} catch(ValueNotFoundException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testAggiungiIdSmellValido() throws Exception {
		try
		{
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.aggiungiSmell(GestoreImpostazioni.LargeClass);
			assertTrue(true);
			assertEquals(1, gestoreImpostazioni.listaSmell().size());
			assertEquals("LargeClass", gestoreImpostazioni.listaSmell().get(0).getName());
		} catch(ValueNotFoundException e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testRimuoviIdSmellNull() throws Exception {
		try
		{
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.rimuoviSmell(null);
			assertTrue(false);
		} catch(ParameterNotNullException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testRimuoviIdSmellNonValido() throws Exception {
		try
		{
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.rimuoviSmell(4);
			assertTrue(false);
		} catch(ValueNotFoundException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testRimuoviIdSmellValido() throws Exception {
		try
		{
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.aggiungiSmell(GestoreImpostazioni.LargeClass);
			assertTrue(true);
			assertEquals(1, gestoreImpostazioni.listaSmell().size());
			assertEquals("LargeClass", gestoreImpostazioni.listaSmell().get(0).getName());
			gestoreImpostazioni.rimuoviSmell(GestoreImpostazioni.LargeClass);
			assertTrue(true);
			assertEquals(0, gestoreImpostazioni.listaSmell().size());
		} catch(ValueNotFoundException e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testListaSmell() throws Exception {
		try
		{
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.aggiungiSmell(GestoreImpostazioni.LargeClass);
			gestoreImpostazioni.aggiungiSmell(GestoreImpostazioni.LongParameterList);
			gestoreImpostazioni.aggiungiSmell(GestoreImpostazioni.LongMethod);
			assertTrue(true);
			assertEquals(3, gestoreImpostazioni.listaSmell().size());
			assertEquals("LargeClass", gestoreImpostazioni.listaSmell().get(0).getName());
			assertEquals("LongParameterList", gestoreImpostazioni.listaSmell().get(1).getName());
			assertEquals("LongMethod", gestoreImpostazioni.listaSmell().get(2).getName());
		} catch(ValueNotFoundException e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testListaSoglieIdSmellNull() throws Exception {
		try
		{
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.listaSoglie(null);
			assertTrue(false);
		} catch(ParameterNotNullException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testListaSoglieIdSmellNonValido() throws Exception {
		try
		{
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.listaSoglie(4);
			assertTrue(false);
		} catch(ValueNotFoundException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testListaSoglieIdSmellValido() throws Exception {
		try
		{
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.aggiungiSmell(GestoreImpostazioni.LargeClass);
			assertTrue(true);
			assertEquals(1, gestoreImpostazioni.listaSoglie(GestoreImpostazioni.LargeClass).size());
			assertEquals(Integer.MAX_VALUE, gestoreImpostazioni.listaSoglie(GestoreImpostazioni.LargeClass).get(0).intValue());
		} catch(ValueNotFoundException e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testGetSogliaIdSmellNUll() throws Exception {
		try
		{
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.getSoglia(null, 1);
			assertTrue(false);
		} catch(ParameterNotNullException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testGetSogliaIdSmellNonValido() throws Exception {
		try
		{
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.getSoglia(4, 1);
			assertTrue(false);
		} catch(ValueNotFoundException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testGetSogliaIdSmellValidoAndIdSogliaNull() throws Exception {
		try
		{
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.aggiungiSmell(GestoreImpostazioni.LargeClass);
			gestoreImpostazioni.getSoglia(GestoreImpostazioni.LargeClass, null).intValue();
			assertTrue(false);
		} catch(ParameterNotNullException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testGetSogliaIdSmellValidoAndIdSogliaNonValida() throws Exception {
		try
		{
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.aggiungiSmell(GestoreImpostazioni.LargeClass);
			gestoreImpostazioni.getSoglia(GestoreImpostazioni.LargeClass, 2).intValue();
			assertTrue(false);
		} catch(ValueNotFoundException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testGetSogliaIdSmellValidoAndIdSogliaValida() throws Exception {
		try
		{
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.aggiungiSmell(GestoreImpostazioni.LargeClass);
			assertTrue(true);
			assertEquals(Integer.MAX_VALUE, gestoreImpostazioni.getSoglia(GestoreImpostazioni.LargeClass, 1).intValue());
		} catch(ValueNotFoundException e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testModificaSogliaIdSmellNull() throws Exception {
		try
		{
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.modificaSoglia(null, 1, 2);
			assertTrue(false);
		} catch(ParameterNotNullException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testModificaSogliaIdSmellNonValido() throws Exception {
		try
		{
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.modificaSoglia(4, 1, 2);
			assertTrue(false);
		} catch(ValueNotFoundException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testModificaSogliaIdSmellValidoAndIdSogliaNull() throws Exception {
		try
		{
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.aggiungiSmell(GestoreImpostazioni.LargeClass);
			assertEquals(Integer.MAX_VALUE, gestoreImpostazioni.getSoglia(GestoreImpostazioni.LargeClass, 1).intValue());
			gestoreImpostazioni.modificaSoglia(GestoreImpostazioni.LargeClass, null, 10);
			assertTrue(false);
		} catch(ParameterNotNullException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testModificaSogliaIdSmellValidoAndIdSogliaNonValida() throws Exception {
		try
		{
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.aggiungiSmell(GestoreImpostazioni.LargeClass);
			assertEquals(Integer.MAX_VALUE, gestoreImpostazioni.getSoglia(GestoreImpostazioni.LargeClass, 1).intValue());
			gestoreImpostazioni.modificaSoglia(GestoreImpostazioni.LargeClass, 2, 10);
			assertTrue(false);
		} catch(ValueNotFoundException e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testModificaSogliaIdSmellValidoAndIdSogliaValidaAndNuovoValoreNull() throws Exception {
		try
		{
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.aggiungiSmell(GestoreImpostazioni.LargeClass);
			assertTrue(true);
			assertEquals(Integer.MAX_VALUE, gestoreImpostazioni.getSoglia(GestoreImpostazioni.LargeClass, 1).intValue());
			gestoreImpostazioni.modificaSoglia(GestoreImpostazioni.LargeClass, 1, null);
			assertTrue(false);
		} catch(ParameterNotNullException e) {
			e.printStackTrace();
			assertTrue(true);
		}
	}
	
	@Test
	public void testModificaSogliaIdSmellValidoAndIdSogliaValida() throws Exception {
		try
		{
			GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
			gestoreImpostazioni.aggiungiSmell(GestoreImpostazioni.LargeClass);
			assertTrue(true);
			assertEquals(Integer.MAX_VALUE, gestoreImpostazioni.getSoglia(GestoreImpostazioni.LargeClass, 1).intValue());
			gestoreImpostazioni.modificaSoglia(GestoreImpostazioni.LargeClass, 1, 10);
			assertEquals(10, gestoreImpostazioni.getSoglia(GestoreImpostazioni.LargeClass, 1).intValue());
		} catch(ValueNotFoundException e) {
			e.printStackTrace();
			assertTrue(false);
		}
	}
	
	@Test
	public void testImportaImpostazioni() throws Exception {
		// TODO
	}
	
	@Test
	public void testEsportaImpostazioni() throws Exception {
		// TODO
	}
}
