package funzionali;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

import jmav.component.GestoreRisultati;
import jmav.exception.ParameterNotNullException;
import jmav.object.OccorrenzaSmell;
import jmav.object.Posizione;
import jmav.object.Smell;
import junit.framework.TestCase;
import metriche.LOCTest;
import metriche.UtilTest;

import org.junit.Test;

public class GestoreRisultatiTest extends TestCase {
	@Test
	public void testRestituisciRisultatoOccorrenzaSmellNull() throws Exception {
		try
		{
			GestoreRisultati gestoreRisultati = new GestoreRisultati();
			gestoreRisultati.restituisciRisultato(null);
			assertTrue(false);
		} catch(Exception e) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testRestituisciRisultatoOccorrenzaSmellNotNull() throws Exception {
		try
		{
			Posizione posizione = UtilTest.getClasse(LOCTest.ClassWith2LinesOfCode);
			OccorrenzaSmell occorrenzaSmell = new OccorrenzaSmell(posizione, true, "LargeClass");
			
			GestoreRisultati gestoreRisultati = new GestoreRisultati();
			gestoreRisultati.restituisciRisultato(occorrenzaSmell);
			assertTrue(true);
		} catch(Exception e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testRestituisciRisultatiTrue() throws Exception {
		try
		{
			Posizione posizione = UtilTest.getClasse(LOCTest.ClassWith2LinesOfCode);
			OccorrenzaSmell occorrenzaSmell = new OccorrenzaSmell(posizione, true, "LargeClass");
			
			GestoreRisultati gestoreRisultati = new GestoreRisultati();
			gestoreRisultati.restituisciRisultato(occorrenzaSmell);
			assertTrue(true);
			Object result = gestoreRisultati.restituisciRisultati(true);
			System.out.println("result: "+result);
			assertEquals(true, result instanceof Collection);
			assertEquals(true, ((Collection) result).toArray()[0] instanceof Smell);
			System.out.println("result 0: "+((Collection) result).toArray()[0]);
		} catch(Exception e) {
			assertTrue(false);
		}
	}

	@Test
	public void testRestituisciRisultatiFalse() throws Exception {
		try
		{
			Posizione posizione = UtilTest.getClasse(LOCTest.ClassWith2LinesOfCode);
			OccorrenzaSmell occorrenzaSmell = new OccorrenzaSmell(posizione, true, "LargeClass");
			
			GestoreRisultati gestoreRisultati = new GestoreRisultati();
			gestoreRisultati.restituisciRisultato(occorrenzaSmell);
			assertTrue(true);
			Object result = gestoreRisultati.restituisciRisultati(false);
			System.out.println("result: "+result);
			assertEquals(true, result instanceof Set);
			assertEquals(true, ((Set) result).toArray()[0] instanceof Entry);
			assertEquals(true, ((Entry) ((Set) result).toArray()[0]).getKey() instanceof Posizione);
			assertEquals(true, ((Entry) ((Set) result).toArray()[0]).getValue() instanceof List);
		} catch(Exception e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testEsportaRisultatiFileNUll() throws Exception {
		try
		{
			Posizione posizione = UtilTest.getClasse(LOCTest.ClassWith2LinesOfCode);
			OccorrenzaSmell occorrenzaSmell = new OccorrenzaSmell(posizione, true, "LargeClass");
			
			GestoreRisultati gestoreRisultati = new GestoreRisultati();
			gestoreRisultati.restituisciRisultato(occorrenzaSmell);
			assertTrue(true);
			String nameFile = "test.csv";
			gestoreRisultati.esportaRisultati(null);
			assertTrue(true);
			BufferedReader br = new BufferedReader(new FileReader(nameFile));
			String sCurrentLine;
			int numRows = 0;
			System.out.println("testEsportaRisultatiOnlyClass: ");
			while ((sCurrentLine = br.readLine()) != null) {
				System.out.println(sCurrentLine);
				numRows++;
			}
			br.close();
			assertEquals(2, numRows);
			assertTrue(false);
		} catch(ParameterNotNullException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testEsportaRisultatiOnlyClass() throws Exception {
		try
		{
			Posizione posizione = UtilTest.getClasse(LOCTest.ClassWith2LinesOfCode);
			OccorrenzaSmell occorrenzaSmell = new OccorrenzaSmell(posizione, true, "LargeClass");
			
			GestoreRisultati gestoreRisultati = new GestoreRisultati();
			gestoreRisultati.restituisciRisultato(occorrenzaSmell);
			assertTrue(true);
			String nameFile = "test.csv";
			gestoreRisultati.esportaRisultati(nameFile);
			assertTrue(true);
			BufferedReader br = new BufferedReader(new FileReader(nameFile));
			String sCurrentLine;
			int numRows = 0;
			System.out.println("testEsportaRisultatiOnlyClass: ");
			while ((sCurrentLine = br.readLine()) != null) {
				System.out.println(sCurrentLine);
				numRows++;
			}
			br.close();
			assertEquals(2, numRows);
			assertTrue(true);
		} catch(Exception e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testEsportaRisultatiOnlyMethod() throws Exception {
		try
		{
			Posizione posizione = UtilTest.getMetodo(LOCTest.MethodWith2LinesOfCode);
			OccorrenzaSmell occorrenzaSmell = new OccorrenzaSmell(posizione, true, "LongMethod");
			
			GestoreRisultati gestoreRisultati = new GestoreRisultati();
			gestoreRisultati.restituisciRisultato(occorrenzaSmell);
			assertTrue(true);
			String nameFile = "test.csv";
			gestoreRisultati.esportaRisultati(nameFile);
			assertTrue(true);
			BufferedReader br = new BufferedReader(new FileReader(nameFile));
			String sCurrentLine;
			int numRows = 0;
			System.out.println("testEsportaRisultatiOnlyMethod: ");
			while ((sCurrentLine = br.readLine()) != null) {
				System.out.println(sCurrentLine);
				numRows++;
			}
			br.close();
			assertEquals(2, numRows);
			assertTrue(true);
		} catch(Exception e) {
			assertTrue(false);
		}
	}
	
	@Test
	public void testEsportaRisultatiClassAndMethod() throws Exception {
		try
		{
			Posizione posizione = UtilTest.getClasse(LOCTest.ClassWith2LinesOfCode);
			OccorrenzaSmell occorrenzaSmell = new OccorrenzaSmell(posizione, true, "LargeClass");
			Posizione posizione2 = UtilTest.getMetodo(LOCTest.MethodWith2LinesOfCode);
			OccorrenzaSmell occorrenzaSmell2 = new OccorrenzaSmell(posizione2, true, "LongMethod");
			
			GestoreRisultati gestoreRisultati = new GestoreRisultati();
			gestoreRisultati.restituisciRisultato(occorrenzaSmell);
			gestoreRisultati.restituisciRisultato(occorrenzaSmell2);
			assertTrue(true);
			String nameFile = "test.csv";
			gestoreRisultati.esportaRisultati(nameFile);
			assertTrue(true);
			BufferedReader br = new BufferedReader(new FileReader(nameFile));
			String sCurrentLine;
			int numRows = 0;
			System.out.println("testEsportaRisultatiClassAndMethod: ");
			while ((sCurrentLine = br.readLine()) != null) {
				System.out.println(sCurrentLine);
				numRows++;
			}
			br.close();
			assertEquals(5, numRows);
			assertTrue(true);
		} catch(Exception e) {
			assertTrue(false);
		}
	}
}
