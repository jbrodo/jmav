package jmav.component;


import java.util.HashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import jmav.exception.ParameterNotNullException;
import jmav.metricCalculator.AgenteCalcolatoreMetrica;
import jmav.metricCalculator.LOCAgent;
import jmav.metricCalculator.NOPARAgent;
import jmav.object.Metrica;
import jmav.object.Posizione;

import org.junit.Test;


public class CalcolatoreMetriche {
	private HashMap<Metrica, HashMap<Posizione, Double>> valoriMetriche;
	private HashMap<Metrica, Boolean> inProgress;
	private ExecutorService pool;
	
	public static final int MAX = 2;
	
	public CalcolatoreMetriche() {
		valoriMetriche = new HashMap<Metrica, HashMap<Posizione, Double>>();
		inProgress = new HashMap<Metrica, Boolean>();
		resetValoriMetriche();
		pool = Executors.newFixedThreadPool(MAX);
	}
	
	public void resetValoriMetriche() {
		valoriMetriche = new HashMap<Metrica, HashMap<Posizione, Double>> ();
		
		for(Metrica metrica : Metrica.values()) {
			valoriMetriche.put(metrica, new HashMap<Posizione,Double>());
			inProgress.put(metrica, false);
		}
	}
	
	public synchronized Double calcolaMatrica(Metrica metrica, Posizione posizione) throws ParameterNotNullException, InterruptedException, ExecutionException {
		if(metrica == null)
			throw new ParameterNotNullException("Parametro metrica non pu� essere null");
		
		if(posizione == null)
			throw new ParameterNotNullException("Parametro posizione non pu� essere null");
		
		if(valoriMetriche.get(metrica).get(posizione) != null) {
			return valoriMetriche.get(metrica).get(posizione);
		}
		
		if(inProgress.get(metrica)) {
			// wait
		}
		
		// Launch/get value
		inProgress.put(metrica, true);
		AgenteCalcolatoreMetrica agent = null;
		
		if(metrica.equals(Metrica.LOC)) {
			agent = new LOCAgent(posizione);
			
		}
		
		if(metrica.equals(Metrica.NOPAR)) {
			agent = new NOPARAgent(posizione);
		}
		
		pool.execute(agent.getJob());
		Double valore = agent.getJob().get().doubleValue();
		HashMap<Posizione, Double> h1 = new HashMap<Posizione, Double>(); 
		h1.put(posizione, valore);
		valoriMetriche.put(metrica, h1);
		inProgress.put(metrica, false);
		return valoriMetriche.get(metrica).get(posizione);
	}
	
	public void shutdown() {
		pool.shutdownNow();
	}
	
	@Test
	public synchronized void interrompiCalcoloMetriche() throws Exception {
		// TODO
	}
}