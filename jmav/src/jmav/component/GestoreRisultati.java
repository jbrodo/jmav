package jmav.component;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;

import jmav.exception.ParameterNotNullException;
import jmav.object.Granularita;
import jmav.object.OccorrenzaSmell;
import jmav.object.Posizione;
import jmav.object.Smell;

public class GestoreRisultati {
	private HashMap<String, Smell> smells;
	private HashMap<Posizione, List<String>> elementi;
	private HashMap<Posizione, List<OccorrenzaSmell>> elementiExport;
	private HashSet<Granularita> granularities;

	public GestoreRisultati() {
		resetRisultati();
	}

	public void restituisciRisultato(OccorrenzaSmell occorrenzaSmell) throws ParameterNotNullException {
		if (occorrenzaSmell == null) {
			throw new ParameterNotNullException("Parametro occorrenzaSmell non pu� essere null");
		}

		if (smells.get(occorrenzaSmell.getSmell()) == null) {
			smells.put(occorrenzaSmell.getSmell(),
					new Smell(occorrenzaSmell.getSmell()));
		}

		smells.get(occorrenzaSmell.getSmell()).addChild(occorrenzaSmell);

		if (occorrenzaSmell.isYes()) {
			if (elementi.get(occorrenzaSmell.getPosizione()) == null) {
				elementi.put(occorrenzaSmell.getPosizione(),
						new ArrayList<String>());
			}

			elementi.get(occorrenzaSmell.getPosizione()).add(
					occorrenzaSmell.getSmell());
		}

		if (elementiExport.get(occorrenzaSmell.getPosizione()) == null) {
			elementiExport.put(occorrenzaSmell.getPosizione(),
					new ArrayList<OccorrenzaSmell>());
		}

		elementiExport.get(occorrenzaSmell.getPosizione()).add(occorrenzaSmell);
		granularities.add(occorrenzaSmell.getPosizione().getGranularita());
	}

	public Object restituisciRisultati(boolean mode) {
		if (mode) {
			return smells.values();
		} else {
			return elementi.entrySet();
		}
	}

	private void resetRisultati() {
		smells = new HashMap<String, Smell>();
		elementi = new HashMap<Posizione, List<String>>();
		elementiExport = new HashMap<Posizione, List<OccorrenzaSmell>>();
		granularities = new HashSet<Granularita>();
	}

	public void esportaRisultati(String percorso) throws ParameterNotNullException {
		if (percorso == null) {
			throw new ParameterNotNullException("Parametro percorso non pu� essere null");
		}
		
		try {
			FileWriter fw = new FileWriter(percorso);
			String separator = ";";
			boolean first = true;

			for (Granularita granularita : granularities) {
				if (!first)
					fw.write("\n");
				
				String title = "Classe";
				
				if(granularita == Granularita.METHOD)
					title += separator + "Metodo";
				
				String testo = "";
				boolean virgola = true;
				for (Entry<Posizione, List<OccorrenzaSmell>> elemento : elementiExport
						.entrySet()) {
					if (elemento.getKey().getGranularita() != granularita)
						continue;

					testo += elemento.getKey().getClassName();
					
					if(granularita == Granularita.METHOD)
						testo += separator + elemento.getKey().getMethodName();
					
					boolean virgolal = true;
					for (OccorrenzaSmell occorrenzaSmell : elemento.getValue()) {
						if (!title.contains(occorrenzaSmell.getSmell())) {
							if (virgola) {
								title += separator;
							}
							title += occorrenzaSmell.getSmell();
						}
						if (virgolal) {
							testo += separator;
						}
						virgolal = true;
						testo += occorrenzaSmell.isYes();
					}
					testo += "\n";
				}
				fw.write(title + "\n");
				fw.write(testo);
				fw.flush();
				first = false;
			}

			fw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}