package jmav.component;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import jmav.exception.ParameterNotNullException;
import jmav.exception.ValueNotFoundException;
import jmav.object.Smell;

public class GestoreImpostazioni {
	
	private HashMap<Integer, Smell> smells;
	private HashMap<Integer, HashMap<Integer, Integer>> soglie;
	
	public static Integer LargeClass = 1;
	public static Integer LongParameterList = 2;
	public static Integer LongMethod = 3;
	
	public GestoreImpostazioni() {
		smells = new HashMap<Integer, Smell>();
		soglie = new HashMap<Integer, HashMap<Integer, Integer>>();
	}
	
	public void importaImpostazioni(File file) {
		//TODO
	}
	
	public void esportaImpostazioni(File file) {
		//TODO
	}
	
	public void aggiungiSmell(Integer idSmell) throws Exception {
		if(idSmell == null) {
			throw new ParameterNotNullException("Parametro idSmell non pu� essere null");
		}
		
		Smell smell = null;
		
		if(idSmell == LargeClass) {
			smell = new Smell("LargeClass");
		}
		if(idSmell == LongParameterList) {
			smell = new Smell("LongParameterList");
		}
		if(idSmell == LongMethod) {
			smell = new Smell("LongMethod");
		}
		
		if(smell != null) {
			smells.put(idSmell, smell);
			HashMap<Integer, Integer> soglia = new HashMap<Integer, Integer>();
			soglia.put(1, Integer.MAX_VALUE);
			soglie.put(idSmell, soglia);
		} else {
			throw new ValueNotFoundException("Valore idSmell non trovato");
		}
	}
	
	public void rimuoviSmell(Integer idSmell) throws Exception {
		if(idSmell == null) {
			throw new ParameterNotNullException("Parametro idSmell non pu� essere null");
		}
		
		Smell smell = smells.remove(idSmell);
		
		if(smell != null) {
			soglie.remove(idSmell);
		} else {
			throw new ValueNotFoundException("Valore idSmell non trovato");
		}
	}
	
	public List<Smell> listaSmell() {
		return new ArrayList<Smell>(smells.values());
	}
	
	public List<Integer> listaSoglie(Integer idSmell) throws ParameterNotNullException, ValueNotFoundException {
		if(idSmell == null)
			throw new ParameterNotNullException("Parametro idSmell non pu� essere null");
		
		HashMap<Integer,Integer> soglieSmell = soglie.get(idSmell);
		
		if(soglieSmell != null) {
			return new ArrayList<Integer>(soglieSmell.values());
		} else {
			throw new ValueNotFoundException("Valore idSmell non trovato");
		}
	}
	
	public Integer getSoglia(Integer idSmell, Integer idSoglia) throws ParameterNotNullException, ValueNotFoundException {
		if(idSmell == null)
			throw new ParameterNotNullException("Parametro idSmell non pu� essere null");
		
		if(idSoglia == null)
			throw new ParameterNotNullException("Parametro idSoglia non pu� essere null");
		
		HashMap<Integer,Integer> soglieSmell = soglie.get(idSmell);
		if(soglieSmell != null) {
			Integer soglia = soglieSmell.get(idSoglia);
			if(soglia != null) {
				return soglia;
			} else {
				throw new ValueNotFoundException("Valore idSoglia non trovato");
			}
		} else {
			throw new ValueNotFoundException("Valore idSmell non trovato");
		}
	}
	
	public void modificaSoglia(Integer idSmell, Integer idSoglia, Integer nuovoValore) throws Exception {
		if(idSmell == null)
			throw new ParameterNotNullException("Parametro idSmell non pu� essere null");
		
		if(idSoglia == null)
			throw new ParameterNotNullException("Parametro idSoglia non pu� essere null");
		
		if(nuovoValore == null)
			throw new ParameterNotNullException("Parametro nuovoValore non pu� essere null");
		
		HashMap<Integer,Integer> soglieSmell = soglie.get(idSmell);
		if(soglieSmell != null) {
			Integer soglia = soglieSmell.get(idSoglia);
			if(soglia != null) {
				soglie.get(idSmell).put(idSoglia, nuovoValore);
			} else {
				throw new ValueNotFoundException("Valore idSoglia non trovato");
			}
		} else {
			throw new ValueNotFoundException("Valore idSmell non trovato");
		}
	}
}
