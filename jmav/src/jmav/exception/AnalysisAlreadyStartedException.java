package jmav.exception;

public class AnalysisAlreadyStartedException extends Exception {
	public AnalysisAlreadyStartedException(String message) {
		super(message);
	}
}
