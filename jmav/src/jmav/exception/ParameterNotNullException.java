package jmav.exception;

public class ParameterNotNullException extends Exception {
	public ParameterNotNullException(String message) {
		super(message);
	}
}
