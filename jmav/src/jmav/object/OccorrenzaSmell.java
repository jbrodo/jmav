package jmav.object;


public class OccorrenzaSmell {
	private String smell;
	private Posizione posizione;
	private boolean yes;

	public OccorrenzaSmell(Posizione posizione, boolean yes, String smell) {
		this.setPosizione(posizione);
		this.setYes(yes);
		this.setSmell(smell);
	}

	public boolean isYes() {
		return yes;
	}

	private void setYes(boolean yes) {
		this.yes = yes;
	}

	public String getName() {
		return posizione.getName();
	}

	public String toString() {
		return "(" + getSmell() + "," + getName() + "," + isYes() + ")";
	}

	public String getSmell() {
		return smell;
	}

	public void setSmell(String smell) {
		this.smell = smell;
	}

	public Posizione getPosizione() {
		return posizione;
	}

	public void setPosizione(Posizione posizione) {
		this.posizione = posizione;
	}
}