package jmav.object;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;

public class JavaProject {
	private String root;
	private HashMap<File, char[]> texts;
	private HashMap<File, CompilationUnit> compilationUnits;
	private String extension;

	public JavaProject(String root) throws Exception {
		this.root = root;
		this.extension = ".java";
		this.texts = new HashMap<File, char[]>();
		this.compilationUnits = new HashMap<File, CompilationUnit>();
		
		File folder = new File(root);
		
		if(!folder.exists()) {
			throw new Exception("file "+folder+" not exists");
		}
		
		inspectFile(folder);
		
		for (Entry<File, char[]> unit : texts.entrySet()) {
			CompilationUnit compilationUnit = parse(unit.getValue());
			compilationUnits.put(unit.getKey(), compilationUnit);
		}
	}

	private void inspectFile(File file) throws IOException {
		if(file.isFile()) {
			if(file.getName().endsWith(extension)) {
				texts.put(file, readFile(file));
			}
			return;
		}
		
		File[] listOfFiles = file.listFiles();
		for (int i = 0; i < listOfFiles.length; i++) {
			inspectFile(listOfFiles[i]);
		}
	}
	
	private char[] readFile(File file) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(file));
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            sb.append("\n");
	            line = br.readLine();
	        }
	        char[] dst = new char[sb.length()];
	        sb.getChars(0, sb.length(), dst, 0);
	        return dst;
	    } finally {
	        br.close();
	    }
	}
	
	public String getRoot() {
		return root;
	}
	
	public HashMap<File, CompilationUnit> getCompilationUnits() {
		return compilationUnits;
	}
	
	/**
	 * Reads a ICompilationUnit and creates the AST DOM for manipulating the
	 * Java source file
	 * 
	 * @param unit
	 * @return
	 */
	public static CompilationUnit parse(char[] unit) {
		ASTParser parser = ASTParser.newParser(AST.JLS3);
		parser.setSource(unit);
		parser.setEnvironment(null, null, null, true);
		parser.setResolveBindings(true);
		parser.setStatementsRecovery(true);
		parser.setBindingsRecovery(true);
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setResolveBindings(true);
		return (CompilationUnit) parser.createAST(null);
	}
}
