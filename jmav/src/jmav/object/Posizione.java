package jmav.object;

import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;

public class Posizione {
	
	private ASTNode node;
	private Granularita Granularita;

	public Posizione(ASTNode node, Granularita Granularita) {
		this.setNode(node);
		this.setGranularita(Granularita);
	}

	public ASTNode getNode() {
		return node;
	}

	public void setNode(ASTNode node) {
		this.node = node;
	}
	
	public String getName() {
		if (node instanceof TypeDeclaration) {
			return getClassName();
		}
		if (node instanceof MethodDeclaration) {
			if (((MethodDeclaration) node).getParent() instanceof TypeDeclaration)
				return getClassName() + "." + getMethodName();
		}
		return null;
	}
	
	public String getMethodName() {
		if (node instanceof MethodDeclaration) {
			if (((MethodDeclaration) node).getParent() instanceof TypeDeclaration)
				return ((MethodDeclaration) node).getName().getFullyQualifiedName();
		}
		return "";
	}
	
	public String getClassName() {
		if (node instanceof TypeDeclaration) {
			return ((TypeDeclaration) node).getName().getFullyQualifiedName();
		}
		if (node instanceof MethodDeclaration) {
			if (((MethodDeclaration) node).getParent() instanceof TypeDeclaration)
				return ((TypeDeclaration) ((MethodDeclaration) node)
						.getParent()).getName().getFullyQualifiedName();
		}
		return "";
	}

	public Granularita getGranularita() {
		return Granularita;
	}

	public void setGranularita(Granularita Granularita) {
		this.Granularita = Granularita;
	}
}
