package jmav.metricCalculator;

import jmav.object.Posizione;

import org.eclipse.jdt.core.dom.MethodDeclaration;

public class NOPARAgent extends AgenteCalcolatoreMetrica {

	public NOPARAgent(Posizione posizione) {
		super(posizione);
	}

	@Override
	Integer calcolaValoriMetrica(Posizione posizione) throws Exception {
		if(posizione.getNode() instanceof MethodDeclaration) {
			return ((MethodDeclaration) posizione.getNode()).parameters().size();
		}
		throw new Exception("Errore metrica non calcolabile in questa posizione");
	}
}
