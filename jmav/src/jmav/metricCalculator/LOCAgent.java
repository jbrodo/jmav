package jmav.metricCalculator;

import jmav.object.Posizione;

import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;

public class LOCAgent extends AgenteCalcolatoreMetrica {

	public LOCAgent(Posizione posizione) {
		super(posizione);
	}

	@Override
	Integer calcolaValoriMetrica(Posizione posizione) throws Exception {
		if(posizione.getNode() instanceof TypeDeclaration) {
			int numberOfLines = computeNumberOfLines(posizione.getNode().toString());
			return numberOfLines;
		}
		if(posizione.getNode() instanceof MethodDeclaration) {
			int numberOfLines = computeNumberOfLines(posizione.getNode().toString());
			return numberOfLines;
		}
		throw new Exception("Errore metrica non calcolabile in questa posizione");
	}

	private int computeNumberOfLines(String text) {
		int count = 0;
		int start = 0;

		String source = text;

		int upComment = nextUpComment(source, start);

		// Delete multi line comment
		while (upComment > -1) {
			int downComment = nextDownComment(source, upComment);
			source = source.substring(0, upComment - 2)
					+ source.substring(downComment);
			upComment = nextUpComment(source, start);
		}

		// Compute line of codes
		int nextStart = nextDelimiterInfo(source, start);
		while (nextStart > -1) {
			String line = source.substring(start, nextStart).trim();
			if (!line.isEmpty() && !line.equals("\n") && !line.equals("\r")
					&& !line.equals("\r\n") && !line.startsWith("//")) {
				++count;
			}
			start = nextStart;
			nextStart = nextDelimiterInfo(source, start);
		}
		return count;
	}

	private int nextDelimiterInfo(String text, int offset) {
		char ch;
		int length = text.length();
		for (int i = offset; i < length; i++) {
			ch = text.charAt(i);
			if (ch == '\r') {

				if (i + 1 < length) {
					if (text.charAt(i + 1) == '\n') {
						return i + 2;
					}
				}
				return i + 1;

			} else if (ch == '\n') {
				return i + 1;
			}
		}

		return -1;
	}

	private int nextUpComment(String text, int offset) {
		char ch;
		int length = text.length();
		for (int i = offset; i < length; i++) {
			ch = text.charAt(i);
			if (ch == '/') {
				if (i + 1 < length) {
					if (text.charAt(i + 1) == '*') {
						return i + 2;
					}
				}
			}
		}

		return -1;
	}

	private int nextDownComment(String text, int offset) {
		char ch;
		int length = text.length();
		for (int i = offset; i < length; i++) {
			ch = text.charAt(i);
			if (ch == '*') {
				if (i + 1 < length) {
					if (text.charAt(i + 1) == '/') {
						return i + 2;
					}
				}
			}
		}

		return -1;
	}
}
