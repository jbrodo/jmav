package jmav.metricCalculator;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

import jmav.object.Posizione;

public abstract class AgenteCalcolatoreMetrica {
	protected FutureTask<Integer> job;
	
	abstract Integer calcolaValoriMetrica(Posizione posizione) throws Exception;
	
	public AgenteCalcolatoreMetrica(final Posizione posizione) {
		Callable<Integer> call = new Callable<Integer>() {
            public Integer call() throws Exception {
				return calcolaValoriMetrica(posizione);
            }
        };
		
        job = new FutureTask<Integer>(call);
	}
	
	public FutureTask<Integer> getJob() {
		return job;
	}
}