package jmav.smellFinder;

import jmav.component.CalcolatoreMetriche;
import jmav.object.Granularita;
import jmav.object.Metrica;
import jmav.object.OccorrenzaSmell;

public class LongMethodAgent extends RilevaSmell {

	private final String nameSmell = "LongMethod";

	public LongMethodAgent(CalcolatoreMetriche calcolatoreMetriche) {
		super(calcolatoreMetriche, "Rileva Long Method");
	}

	@Override
	protected OccorrenzaSmell individuaSmell(CalcolatoreMetriche calcolatoreMetriche) throws Exception {
		Double valore = calcolatoreMetriche.calcolaMatrica(getMetrica(), getPosizione());
		return new OccorrenzaSmell(getPosizione(), valore > soglia, nameSmell);
	}
	
	public static Granularita getGranularita() {
		return Granularita.METHOD;
	}
	
	protected Metrica getMetrica() {
		return Metrica.LOC;
	}
}
