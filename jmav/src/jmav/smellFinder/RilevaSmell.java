package jmav.smellFinder;


import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

import jmav.component.CalcolatoreMetriche;
import jmav.object.Metrica;
import jmav.object.OccorrenzaSmell;
import jmav.object.Posizione;

public abstract class RilevaSmell {
	 
	protected FutureTask<OccorrenzaSmell> job;
	protected int soglia;
	protected Posizione posizione;
	
	public RilevaSmell(final CalcolatoreMetriche calcolatoreMetriche, String name) {
		Callable<OccorrenzaSmell> call = new Callable<OccorrenzaSmell>() {
            public OccorrenzaSmell call() {
            	OccorrenzaSmell smell = null;
				try {
					smell = individuaSmell(calcolatoreMetriche);
				} catch (Exception e) {
					e.printStackTrace();
				}
                return smell;
            }
        };
		
        job = new FutureTask<OccorrenzaSmell>(call);
	}

	public void setSoglia(int soglia) {
		this.soglia = soglia;
	}
	
	public FutureTask<OccorrenzaSmell> getJob() {
		return job;
	}
	
	protected abstract OccorrenzaSmell individuaSmell(CalcolatoreMetriche calcolatoreMetriche) throws Exception;
	protected abstract Metrica getMetrica();

	public Posizione getPosizione() {
		return posizione;
	}

	public void setPosizione(Posizione posizione) {
		this.posizione = posizione;
	}
}