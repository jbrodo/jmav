package jmav.smellFinder;

import jmav.component.CalcolatoreMetriche;
import jmav.object.Granularita;
import jmav.object.Metrica;
import jmav.object.OccorrenzaSmell;

public class LargeClassAgent extends RilevaSmell {

	private final String nameSmell = "LargeClass";

	public LargeClassAgent(CalcolatoreMetriche calcolatoreMetriche) {
		super(calcolatoreMetriche, "Rileva Large Class");
	}

	protected OccorrenzaSmell individuaSmell(CalcolatoreMetriche calcolatoreMetriche) throws Exception {
		Double valore = calcolatoreMetriche.calcolaMatrica(getMetrica(), getPosizione());
		return new OccorrenzaSmell(getPosizione(), valore > soglia, nameSmell);
	}
	
	public static Granularita getGranularita() {
		return Granularita.CLASS;
	}
	
	protected Metrica getMetrica() {
		return Metrica.LOC;
	}
}
