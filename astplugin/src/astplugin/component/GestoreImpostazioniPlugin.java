package astplugin.component;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import astplugin.Activator;
import astplugin.ui.BooleanFieldEditorExt;

public class GestoreImpostazioniPlugin extends FieldEditorPreferencePage
		implements IWorkbenchPreferencePage {

	public GestoreImpostazioniPlugin() {
		super(GRID);
	}

	public void createFieldEditors() {
		BooleanFieldEditorExt bvLargeClass = new BooleanFieldEditorExt(
				"BV_LARGE_CLASS",
				"Large Class",
				getFieldEditorParent(),
				"Large Class",
				"./imgs/largeclass.png",
				"Questo smell occorre quando ad una classe viene assegnato un numero eccessivo di funzionalit�.\n"
						+ "Spesso in questi casi la classe contiene un numero molto elevato di variabili d'istanza.\n\n", 
						"LOC - Lines Of Code",
						"Il numero di linee di codice di un metodo, classe, package o progetto.");
		IntegerFieldEditor sgLargeClass = new IntegerFieldEditor(
				"SG_LARGE_CLASS", "Soglia 1:", getFieldEditorParent());
		bvLargeClass.setEnabledField(sgLargeClass);

		BooleanFieldEditorExt bvLongParList = new BooleanFieldEditorExt(
				"BV_LONG_PAR_LIST",
				"Long Parameter List",
				getFieldEditorParent(),
				"Long Parameter List",
				"./imgs/longparameterlist.png",
				"In un linguaggio di programmazione orientato agli oggetti se un metodo non ha a disposizione tutto ci� \n" +
				"di cui necessita pu� richiederlo ad altri oggetti.\n Per questo motivo non � necessario passare ad un metodo " +
				"tutto ci� di cui ha bisogno, ma solo ci� che\ngli � indispensabile per ottenere quello di cui necessita.\n" +
				"Si ha una occorrenza di Long Parameter List quando ad un metodo viene passato un numero eccessivo di parametri.\n\n", 
				"NOPAR - Number Of Parameters",
				"Il numero di parametri passati a un metodo.");
		IntegerFieldEditor sgLongParList = new IntegerFieldEditor(
				"SG_LONG_PAR_LIST", "Soglia 1:",
				getFieldEditorParent());
		bvLongParList.setEnabledField(sgLongParList);

		BooleanFieldEditorExt bvLongMethod = new BooleanFieldEditorExt(
				"BV_LONG_METHOD", "Long Method", getFieldEditorParent(),
				"Long Method",
				"./imgs/longmethod.png",
				"Questo smell occorre quando ad un metodo ha un numero eccessivo di righe.\n"
						+ "Spesso in questi casi il metodo � difficile da comprendere e complica la fase di manutenzione del codice.\n", 
						"LOC - Lines Of Code",
						"Il numero di linee di codice di un metodo, classe, package o progetto.");
		IntegerFieldEditor sgLongMethod = new IntegerFieldEditor(
				"SG_LONG_METHOD", "Soglia 1:", getFieldEditorParent());
		bvLongMethod.setEnabledField(sgLongMethod);

		addField(bvLargeClass);
		addField(sgLargeClass);
		addField(bvLongParList);
		addField(sgLongParList);
		addField(bvLongMethod);
		addField(sgLongMethod);
	}

	// checkState allow you to perform validations
	@Override
	protected void checkState() {
		super.checkState();
	}

	public void init(IWorkbench workbench) {
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("Scegli i tuoi smell");
	}
}
