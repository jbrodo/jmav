package astplugin.component;

import jmav.component.CalcolatoreMetriche;
import jmav.component.GestoreAnalisi;
import jmav.component.GestoreImpostazioni;
import jmav.component.GestoreRisultati;
import jmav.object.JavaProject;
import jmav.object.OccorrenzaSmell;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

import astplugin.Activator;

public class GestoreAnalisiPlugin extends AbstractHandler {
	
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Shell shell = HandlerUtil.getActiveShell(event);
		IStructuredSelection selection = (IStructuredSelection) HandlerUtil
				.getActiveMenuSelection(event);

		if (selection == null || selection.getFirstElement() == null) {
			MessageDialog.openInformation(shell, "Informazione",
					"Perfavore seleziona un progetto");
			return null;
		}

		final Object firstElement = selection.getFirstElement();
		if (!(firstElement instanceof IJavaProject)) {
			return null;
		}

		final IJavaProject project = (IJavaProject) firstElement;

		try {
			final GestoreRisultatiPlugin gestoreRisultatiPlugin = (GestoreRisultatiPlugin) HandlerUtil
					.getActiveWorkbenchWindow(event).getActivePage()
					.showView(GestoreRisultatiPlugin.ID);
			
			final String jmav_projectpath = ResourcesPlugin.getWorkspace().getRoot().findMember(project.getPath()).getLocation().toString();
			
			final Job job = new Job("Analisi di "+jmav_projectpath) {
				@Override
				protected IStatus run(IProgressMonitor arg0) {
					try {
						IPreferenceStore store = Activator.getDefault().getPreferenceStore();
						
						final GestoreRisultati jmav_GestoreRisultati = new GestoreRisultati();
						gestoreRisultatiPlugin.setGestoreRisultati(jmav_GestoreRisultati);
						
						Display.getDefault().syncExec(new Runnable() {
							public void run() {
								gestoreRisultatiPlugin.resetInput();
							}
						});
						
						final JavaProject jmav_project = new JavaProject(jmav_projectpath);
						
						GestoreImpostazioni gestoreImpostazioni = new GestoreImpostazioni();
						
						if(store.getBoolean("BV_LARGE_CLASS")) {
							gestoreImpostazioni.aggiungiSmell(GestoreImpostazioni.LargeClass);
							gestoreImpostazioni.modificaSoglia(GestoreImpostazioni.LargeClass, 1, store.getInt("SG_LARGE_CLASS"));
						}
						if(store.getBoolean("BV_LONG_PAR_LIST")) {
							gestoreImpostazioni.aggiungiSmell(GestoreImpostazioni.LongParameterList);
							gestoreImpostazioni.modificaSoglia(GestoreImpostazioni.LongParameterList, 1, store.getInt("SG_LONG_PAR_LIST"));
						}
						if(store.getBoolean("BV_LONG_METHOD")) {
							gestoreImpostazioni.aggiungiSmell(GestoreImpostazioni.LongMethod);
							gestoreImpostazioni.modificaSoglia(GestoreImpostazioni.LongMethod, 1, store.getInt("SG_LONG_METHOD"));
						}
						
						CalcolatoreMetriche calcolatoreMetriche = new CalcolatoreMetriche();
						
						final GestoreAnalisi jmav_GestoreAnalisi = new GestoreAnalisi(gestoreImpostazioni, calcolatoreMetriche);
						
						jmav_GestoreAnalisi.avviaAnalisi(jmav_project);
						
						for(OccorrenzaSmell occorrenzaSmell : jmav_GestoreAnalisi.listaRisultati()) {
							jmav_GestoreRisultati.restituisciRisultato(occorrenzaSmell);
						}
						
						Display.getDefault().syncExec(new Runnable() {
							public void run() {
								gestoreRisultatiPlugin.showInput();
							}
						});
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					return Status.OK_STATUS;
				}
			};
			
			job.setUser(true);
			job.schedule();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}