package astplugin.component;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import astplugin.Activator;

public class GestoreImpostazioniInitializer extends AbstractPreferenceInitializer {

	public GestoreImpostazioniInitializer() {
	}

	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		store.setDefault("BV_LARGE_CLASS", true);
		store.setDefault("SG_LARGE_CLASS", 30);
		store.setDefault("BV_LONG_PAR_LIST", false);
		store.setDefault("SG_LONG_PAR_LIST", 3);
		store.setDefault("BV_LONG_METHOD", true);
		store.setDefault("SG_LONG_METHOD", 10);
	}
}