package astplugin.component;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;

import jmav.component.GestoreRisultati;
import jmav.object.OccorrenzaSmell;
import jmav.object.Posizione;
import jmav.object.Smell;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.part.ViewPart;

import astplugin.Activator;

public class GestoreRisultatiPlugin extends ViewPart {
	public static final String ID = "jmav.GestoreRisultatiPlugin";
	private TreeViewer viewer;
	private boolean choice;
	private GestoreRisultati jmav_GestoreRisultati;
	
	public void setGestoreRisultati(GestoreRisultati GestoreRisultati) {
		this.jmav_GestoreRisultati = GestoreRisultati;
	}

	public void showInput() {
		viewer.setInput(jmav_GestoreRisultati.restituisciRisultati(choice));
	}

	public void resetInput() {
		viewer.setInput(new ArrayList<Object>());
	}

	@Override
	public void createPartControl(Composite parent) {
		viewer = new TreeViewer(parent, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL);
		viewer.setLabelProvider(new AnalysisLabelProvider());
		viewer.setContentProvider(new AnalysisContentProvider());
		viewer.expandAll();

		// Create toolbar
		createToolbar();
	}

	@Override
	public void setFocus() {
	}

	public class AnalysisLabelProvider extends LabelProvider {

		@Override
		public Image getImage(Object element) {
			return null;
		}

		@Override
		public String getText(Object element) {
			if (element instanceof Smell) {
				return ((Smell) element).getName();
			}
			if (element instanceof Entry) {
				return getText(((Entry) element).getKey());
			}
			if(element instanceof OccorrenzaSmell) {
				return ((OccorrenzaSmell) element).getName();
			}
			if (element instanceof String) {
				return ((String) element);
			}
			if (element instanceof Posizione) {
				return ((Posizione) element).getName();
			}
			return null;
		}
	}

	public class AnalysisContentProvider implements ITreeContentProvider {

		@Override
		public Object[] getElements(Object inputElement) {
			Collection list = (Collection) inputElement;
			return list.toArray();
		}

		@Override
		public void dispose() {
		}

		@Override
		public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		}

		@Override
		public Object[] getChildren(Object element) {
			if (element instanceof Smell) {
				List<OccorrenzaSmell> list = ((Smell) element).getChildren();
				return list.toArray();
			}
			if (element instanceof Entry) {
				Collection list = (Collection) ((Entry) element).getValue();
				return list.toArray();
			}
			return null;
		}

		@Override
		public Object getParent(Object arg0) {
			return null;
		}

		@Override
		public boolean hasChildren(Object element) {
			if (element instanceof Smell) {
				return ((Smell) element).getChildren().size() > 0;
			}
			if (element instanceof Entry) {
				Collection list = (Collection) ((Entry) element).getValue();
				return list.size() > 0;
			}
			return false;
		}

	}

	/**
	 * Create toolbar.
	 */
	private void createToolbar() {
		IToolBarManager mgr = getViewSite().getActionBars().getToolBarManager();
		
		Action switchResultAction = new Action("switch") {
			public void run() {
				if(jmav_GestoreRisultati != null) {
					choice = !choice;
					showInput();
				}
			}
		};
		switchResultAction.setImageDescriptor(Activator.getImageDescriptor("icons/view.gif"));
		mgr.add(switchResultAction);
		
		Action exportResultAction = new Action("export") {
			public void run() {
				if(jmav_GestoreRisultati == null) {
					MessageDialog.openInformation(getSite().getShell(), "Informazione", "Perfavore avvia un analisi prima di esportare i risultati");
					return;
				}
				
				String selected = openDialogs();
				if(selected != null) {
					jmav_GestoreRisultati.esportaRisultati(new File(selected));
				}
			}
		};
		exportResultAction.setImageDescriptor(Activator.getImageDescriptor("icons/add.gif"));
		mgr.add(exportResultAction);
	}
	
	private String openDialogs() {
		FileDialog saveDialog = new FileDialog(getSite().getShell(), SWT.SAVE);
		saveDialog.setText("Seleziona una destinazione di esportazione dei risultati");
		String selected = saveDialog.open();
	    return selected;
	}
}