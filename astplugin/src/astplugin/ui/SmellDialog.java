package astplugin.ui;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.layout.LayoutConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import astplugin.Activator;

public class SmellDialog extends TitleAreaDialog {
	private String nameSmell;
	private String imgSmell;
	private String descrizioneSmell;
	private String titoloMetrica;
	private String descrizioneMetrica;

	public SmellDialog(Shell parentShell, String nameSmell, String imgSmell,
			String descrizioneSmell, String titoloMetrica,
			String descrizioneMetrica) {
		super(parentShell);
		this.nameSmell = nameSmell;
		this.imgSmell = imgSmell;
		this.descrizioneSmell = descrizioneSmell;
		this.titoloMetrica = titoloMetrica;
		this.descrizioneMetrica = descrizioneMetrica;
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite contents = new Composite(parent, SWT.NONE);
		contents.setLayout(new RowLayout(SWT.VERTICAL));

		setTitle("Descrizione del code smell: " + nameSmell);

		Label labelTextTop = new Label(contents, SWT.FILL);
		labelTextTop.setText(descrizioneSmell);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
		gridData.horizontalSpan = 2;
		labelTextTop.setLayoutData(gridData);

		Image image = new Image(Display.getDefault(), Activator
				.getImageDescriptor(imgSmell).getImageData());
		Label labelImg = new Label(contents, SWT.FILL);
		labelImg.setImage(image);
		
		Label labelTextDescrizioneMetriche = new Label(contents, SWT.BOLD);
		Font bold2 = new Font(contents.getDisplay(), "Arial", 14, SWT.BOLD);
		labelTextDescrizioneMetriche.setFont(bold2);
		labelTextDescrizioneMetriche.setText("Descrizione delle metriche utilizzate");
		GridData gridDataTitoloMetrica2 = new GridData(SWT.FILL, SWT.FILL, true,
				false);
		gridDataTitoloMetrica2.horizontalSpan = 2;
		labelTextDescrizioneMetriche.setLayoutData(gridDataTitoloMetrica2);

		Label labelTextTitoloMetrica = new Label(contents, SWT.BOLD);
		Font bold = new Font(contents.getDisplay(), "Arial", 12, SWT.BOLD);
		labelTextTitoloMetrica.setFont(bold);
		labelTextTitoloMetrica.setText(titoloMetrica);
		GridData gridDataTitoloMetrica = new GridData(SWT.FILL, SWT.FILL, true,
				false);
		gridDataTitoloMetrica.horizontalSpan = 2;
		labelTextTitoloMetrica.setLayoutData(gridDataTitoloMetrica);

		Label labelTextDescrizioneMetrica = new Label(contents, SWT.FILL);
		labelTextDescrizioneMetrica.setText(descrizioneMetrica);
		GridData gridDataDescrizioneMetrica = new GridData(SWT.FILL, SWT.FILL,
				true, false);
		gridDataDescrizioneMetrica.horizontalSpan = 2;
		labelTextDescrizioneMetrica.setLayoutData(gridDataDescrizioneMetrica);

		Dialog.applyDialogFont(parent);

		Point defaultMargins = LayoutConstants.getMargins();
		GridLayoutFactory.fillDefaults().numColumns(2)
				.margins(defaultMargins.x, defaultMargins.y)
				.generateLayout(contents);

		return contents;
	}

	public static void main(String[] args) {
		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setText("Title Area Shell");
		shell.pack();
		TitleAreaDialog taDialog = new SmellDialog(
				shell,
				"Long Parameter List",
				"./imgs/longparameterlist.png",
				"In un linguaggio di programmazione orientato agli oggetti se un metodo non ha a disposizione tutto ci� \n"
						+ "di cui necessita pu� richiederlo ad altri oggetti.\n Per questo motivo non � necessario passare ad un metodo "
						+ "tutto ci� di cui ha bisogno, ma solo ci� che\ngli � indispensabile per ottenere quello di cui necessita.\n"
						+ "Si ha una occorrenza di Long Parameter List quando ad un metodo viene passato un numero eccessivo di parametri.\n\n",
				"NOPAR", "E' Il numero di parametri di un operazione.");
		taDialog.open();
	}
}