package astplugin.ui;

import org.eclipse.jface.preference.FieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;

/**
 * A field editor for a boolean type preference.
 */
public class BooleanFieldEditorExt extends FieldEditor {
	
	private FieldEditor enabledField;
	private String nameSmell;
	private String imgSmell;
	private String descrizioneSmell;
	private String titoloMetrica;
	private String descrizioneMetrica;
	
	/**
	 * The previously selected, or "before", value.
	 */
	private boolean wasSelected;

	/**
	 * The checkbox control, or <code>null</code> if none.
	 */
	private Button checkBox = null;

	public BooleanFieldEditorExt(String name, String labelText, Composite parent, String nameSmell, String imgSmell, String descrizioneSmell, String titoloMetrica, String descrizioneMetrica) {
		init(name, labelText);
		createControl(parent);
		this.nameSmell = nameSmell;
		this.imgSmell = imgSmell;
		this.descrizioneSmell = descrizioneSmell;
		this.titoloMetrica = titoloMetrica;
		this.descrizioneMetrica = descrizioneMetrica;
	}

	/*
	 * (non-Javadoc) Method declared on FieldEditor.
	 */
	protected void adjustForNumColumns(int numColumns) {
	}

	/*
	 * (non-Javadoc) Method declared on FieldEditor.
	 */
	protected void doFillIntoGrid(Composite parent, int numColumns) {
		String text = getLabelText();
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		gd.horizontalSpan = numColumns;
		
		checkBox = getChangeControl(parent);
		checkBox.setLayoutData(gd);
		if (text != null) {
			checkBox.setText(text);
		}
		
		Button infoButton = getInfoButton(parent);
		infoButton.setLayoutData(gd);
		infoButton.setText("Descrizione");
	}

	public Control getDescriptionControl(Composite parent) {
		return getChangeControl(parent);
	}

	/*
	 * (non-Javadoc) Method declared on FieldEditor. Loads the value from the
	 * preference store and sets it to the check box.
	 */
	protected void doLoad() {
		if (checkBox != null) {
			boolean value = getPreferenceStore().getBoolean(getPreferenceName());
			checkBox.setSelection(value);
			wasSelected = value;
			if(enabledField != null) {
				enabledField.setEnabled(wasSelected, checkBox.getParent());
			}
		}
	}

	/*
	 * (non-Javadoc) Method declared on FieldEditor. Loads the default value
	 * from the preference store and sets it to the check box.
	 */
	protected void doLoadDefault() {
		if (checkBox != null) {
			boolean value = getPreferenceStore().getDefaultBoolean(getPreferenceName());
			checkBox.setSelection(value);
			wasSelected = value;
			if(enabledField != null) {
				enabledField.setEnabled(wasSelected, checkBox.getParent());
			}
		}
	}

	/*
	 * (non-Javadoc) Method declared on FieldEditor.
	 */
	protected void doStore() {
		getPreferenceStore().setValue(getPreferenceName(), checkBox.getSelection());
	}

	/**
	 * Returns this field editor's current value.
	 * 
	 * @return the value
	 */
	public boolean getBooleanValue() {
		return checkBox.getSelection();
	}

	/**
	 * Returns the change button for this field editor.
	 * 
	 * @param parent
	 *            The Composite to create the receiver in.
	 * 
	 * @return the change button
	 */
	protected Button getChangeControl(final Composite parent) {
		if (checkBox == null) {
			checkBox = new Button(parent, SWT.CHECK | SWT.LEFT);
			checkBox.setFont(parent.getFont());
			checkBox.addSelectionListener(new SelectionAdapter() {
				public void widgetSelected(SelectionEvent e) {
					boolean isSelected = checkBox.getSelection();
					if(enabledField != null) {
						enabledField.setEnabled(isSelected, parent);
					}
					valueChanged(wasSelected, isSelected);
					wasSelected = isSelected;
				}
			});
			checkBox.addDisposeListener(new DisposeListener() {
				public void widgetDisposed(DisposeEvent event) {
					checkBox = null;
				}
			});
		} else {
			checkParent(checkBox, parent);
		}
		return checkBox;
	}
	
	protected Button getInfoButton(final Composite parent) {
		final Button infoButton = new Button(parent, SWT.BUTTON1 | SWT.LEFT);
		infoButton.setFont(parent.getFont());
		infoButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				// Open Dialog
				SmellDialog dialog = new SmellDialog(parent.getShell(), nameSmell, imgSmell, descrizioneSmell, titoloMetrica, descrizioneMetrica);
				dialog.open();
			}
		});
		return infoButton;
	}

	/*
	 * (non-Javadoc) Method declared on FieldEditor.
	 */
	public int getNumberOfControls() {
		return 1;
	}

	/*
	 * (non-Javadoc) Method declared on FieldEditor.
	 */
	public void setFocus() {
		if (checkBox != null) {
			checkBox.setFocus();
		}
	}

	/*
	 * (non-Javadoc) Method declared on FieldEditor.
	 */
	public void setLabelText(String text) {
		super.setLabelText(text);
		Label label = getLabelControl();
		if (label == null && checkBox != null) {
			checkBox.setText(text);
		}
	}

	/**
	 * Informs this field editor's listener, if it has one, about a change to
	 * the value (<code>VALUE</code> property) provided that the old and new
	 * values are different.
	 * 
	 * @param oldValue
	 *            the old value
	 * @param newValue
	 *            the new value
	 */
	protected void valueChanged(boolean oldValue, boolean newValue) {
		setPresentsDefaultValue(false);
		if (oldValue != newValue) {
			fireStateChanged(VALUE, oldValue, newValue);
		}
	}

	/*
	 * @see FieldEditor.setEnabled
	 */
	public void setEnabled(boolean enabled, Composite parent) {
		getChangeControl(parent).setEnabled(enabled);
	}

	public void setEnabledField(FieldEditor enabledField) {
		this.enabledField = enabledField;
	}
}
