
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import jmav.component.CalcolatoreMetriche;
import jmav.component.GestoreImpostazioni;
import jmav.object.Granularita;
import jmav.object.JavaProject;
import jmav.object.OccorrenzaSmell;
import jmav.object.Posizione;
import jmav.smellFinder.LargeClassAgent;
import jmav.smellFinder.LongMethodAgent;
import jmav.smellFinder.LongParameterListAgent;
import jmav.visitor.ClassVisitor;
import jmav.visitor.MethodVisitor;

import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;

public class LargeClass {
	private int numSmells;
	private GestoreImpostazioni store;
	private CalcolatoreMetriche calcolatoreMetriche;
	private List<Posizione> posizioni;
	private JavaProject project;
	private List<OccorrenzaSmell> occorrenzaSmells;
	
	public void normalMethodNoPar() {
		posizioni = new ArrayList<Posizione>();
		occorrenzaSmells = new ArrayList<OccorrenzaSmell>();
	}
	
	public void normalMethodFewPar(GestoreImpostazioni store, CalcolatoreMetriche calcolatoreMetriche) {
		this.store = store;
		this.calcolatoreMetriche = calcolatoreMetriche;
		posizioni = new ArrayList<Posizione>();
		occorrenzaSmells = new ArrayList<OccorrenzaSmell>();
	}
	
	public void normalMethodManyPar(GestoreImpostazioni store, CalcolatoreMetriche calcolatoreMetriche, boolean par1, boolean par2, boolean par3) {
		this.store = store;
		this.calcolatoreMetriche = calcolatoreMetriche;
		posizioni = new ArrayList<Posizione>();
		occorrenzaSmells = new ArrayList<OccorrenzaSmell>();
	}
	
	public void longMethodNoPar() throws Exception {
		posizioni = new ArrayList<Posizione>();
		calcolatoreMetriche.resetValoriMetriche();
		occorrenzaSmells = new ArrayList<OccorrenzaSmell>();
		
		numSmells = 0;

		LargeClassAgent largeClassAgent = null;
		LongParameterListAgent longParameterListAgent = null;
		LongMethodAgent longMethodAgent = null;
		
		Set<Granularita> GranularitaSet = new HashSet<Granularita>();

		/* Prendi smell selezionati */
		boolean largeClassBV = store.getBoolean("BV_LARGE_CLASS");
		boolean longParListBV = store.getBoolean("BV_LONG_PAR_LIST");
		boolean longMethodBV = store.getBoolean("BV_LONG_METHOD");
		
		if (largeClassBV) {
			System.out.println("Prepare Large Class Agent");
			numSmells++;
			GranularitaSet.add(LargeClassAgent.getGranularita());
		}
		
		if (longParListBV) {
			System.out.println("Prepare Long Parameter List Agent");
			numSmells++;
			GranularitaSet.add(LongParameterListAgent.getGranularita());
		}
		
		if (longMethodBV) {
			System.out.println("Prepare Long Method Agent");
			numSmells++;
			GranularitaSet.add(LongMethodAgent.getGranularita());
		}
		
		if(numSmells == 0)
			return;
		
		/* Calcolo le posizioni coerenti in base ai livelli di granularità */
		longMethodManyPar(GranularitaSet, true, true, true);
		
		ExecutorService pool = Executors.newFixedThreadPool(numSmells);
		
		if(posizioni != null) {
			for(Posizione posizione : posizioni) {
				if (largeClassBV) {
					if(posizione.getGranularita() == LargeClassAgent.getGranularita()) {
						System.out.println("run Large Class Agent with "+ store.getInteger("SG_LARGE_CLASS"));
						largeClassAgent = new LargeClassAgent(calcolatoreMetriche);
						largeClassAgent.setSoglia(store.getInteger("SG_LARGE_CLASS"));
						largeClassAgent.setPosizione(posizione);
						pool.execute(largeClassAgent.getJob());
					}
				}
		
				if (longParListBV) {
					if(posizione.getGranularita() == LongParameterListAgent.getGranularita()) {
						System.out.println("run Long Parameter List with "+ store.getInteger("SG_LONG_PAR_LIST"));
						longParameterListAgent = new LongParameterListAgent(calcolatoreMetriche);
						longParameterListAgent.setSoglia(store.getInteger("SG_LONG_PAR_LIST"));
						longParameterListAgent.setPosizione(posizione);
						pool.execute(longParameterListAgent.getJob());
					}
				}
				
				if (longMethodBV) {
					if(posizione.getGranularita() == LongParameterListAgent.getGranularita()) {
						System.out.println("run Long Parameter List with "+ store.getInteger("SG_LONG_METHOD"));
						longMethodAgent = new LongMethodAgent(calcolatoreMetriche);
						longMethodAgent.setSoglia(store.getInteger("SG_LONG_METHOD"));
						longMethodAgent.setPosizione(posizione);
						pool.execute(longMethodAgent.getJob());
					}
				}
				
				if (largeClassBV) {
					if(posizione.getGranularita() == LargeClassAgent.getGranularita()) {
						OccorrenzaSmell occorrenzaSmell = largeClassAgent.getJob().get();
						if(occorrenzaSmell.getName() != null) {
							occorrenzaSmells.add(occorrenzaSmell);
							System.out.println("LargeClassAgent aggiunge "+occorrenzaSmell);
						}
					}
				}
				
				if (longParListBV) {
					if(posizione.getGranularita() == LongParameterListAgent.getGranularita()) {
						OccorrenzaSmell occorrenzaSmell = longParameterListAgent.getJob().get();
						if(occorrenzaSmell.getName() != null) {
							occorrenzaSmells.add(occorrenzaSmell);
							System.out.println("LongParameterListAgent aggiunge "+occorrenzaSmell);
						}
					}
				}
				
				if (longMethodBV) {
					if(posizione.getGranularita() == LongParameterListAgent.getGranularita()) {
						OccorrenzaSmell occorrenzaSmell = longMethodAgent.getJob().get();
						if(occorrenzaSmell.getName() != null) {
							occorrenzaSmells.add(occorrenzaSmell);
							System.out.println("LongParameterListAgent aggiunge "+occorrenzaSmell);
						}
					}
				}
			}
		}
		
		pool.shutdown(); // Disable new tasks from being submitted
		try {
			// Wait a while for existing tasks to terminate
			if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
				pool.shutdownNow(); // Cancel currently executing tasks
				// Wait a while for tasks to respond to being cancelled
				if (!pool.awaitTermination(60, TimeUnit.SECONDS))
					System.err.println("Pool did not terminate");
			}
		} catch (InterruptedException ie) {
			// (Re-)Cancel if current thread also interrupted
			pool.shutdownNow();
			// Preserve interrupt status
			Thread.currentThread().interrupt();
		}
		
		calcolatoreMetriche.shutdown();
	}

	public void longMethodFewPar(JavaProject project) throws Exception {
		this.project = project;
		
		posizioni = new ArrayList<Posizione>();
		calcolatoreMetriche.resetValoriMetriche();
		occorrenzaSmells = new ArrayList<OccorrenzaSmell>();
		
		numSmells = 0;

		LargeClassAgent largeClassAgent = null;
		LongParameterListAgent longParameterListAgent = null;
		LongMethodAgent longMethodAgent = null;
		
		Set<Granularita> granularitaSet = new HashSet<Granularita>();

		/* Prendi smell selezionati */
		boolean largeClassBV = store.getBoolean("BV_LARGE_CLASS");
		boolean longParListBV = store.getBoolean("BV_LONG_PAR_LIST");
		boolean longMethodBV = store.getBoolean("BV_LONG_METHOD");
		
		if (largeClassBV) {
			System.out.println("Prepare Large Class Agent");
			numSmells++;
			granularitaSet.add(LargeClassAgent.getGranularita());
		}
		
		if (longParListBV) {
			System.out.println("Prepare Long Parameter List Agent");
			numSmells++;
			granularitaSet.add(LongParameterListAgent.getGranularita());
		}
		
		if (longMethodBV) {
			System.out.println("Prepare Long Method Agent");
			numSmells++;
			granularitaSet.add(LongMethodAgent.getGranularita());
		}
		
		if(numSmells == 0)
			return;
		
		/* Calcolo le posizioni coerenti in base ai livelli di granularità */
		longMethodManyPar(granularitaSet, true, true, true);
		
		ExecutorService pool = Executors.newFixedThreadPool(numSmells);
		
		if(posizioni != null) {
			for(Posizione posizione : posizioni) {
				if (largeClassBV) {
					if(posizione.getGranularita() == LargeClassAgent.getGranularita()) {
						System.out.println("run Large Class Agent with "+ store.getInteger("SG_LARGE_CLASS"));
						largeClassAgent = new LargeClassAgent(calcolatoreMetriche);
						largeClassAgent.setSoglia(store.getInteger("SG_LARGE_CLASS"));
						largeClassAgent.setPosizione(posizione);
						pool.execute(largeClassAgent.getJob());
					}
				}
		
				if (longParListBV) {
					if(posizione.getGranularita() == LongParameterListAgent.getGranularita()) {
						System.out.println("run Long Parameter List with "+ store.getInteger("SG_LONG_PAR_LIST"));
						longParameterListAgent = new LongParameterListAgent(calcolatoreMetriche);
						longParameterListAgent.setSoglia(store.getInteger("SG_LONG_PAR_LIST"));
						longParameterListAgent.setPosizione(posizione);
						pool.execute(longParameterListAgent.getJob());
					}
				}
				
				if (longMethodBV) {
					if(posizione.getGranularita() == LongParameterListAgent.getGranularita()) {
						System.out.println("run Long Parameter List with "+ store.getInteger("SG_LONG_METHOD"));
						longMethodAgent = new LongMethodAgent(calcolatoreMetriche);
						longMethodAgent.setSoglia(store.getInteger("SG_LONG_METHOD"));
						longMethodAgent.setPosizione(posizione);
						pool.execute(longMethodAgent.getJob());
					}
				}
				
				if (largeClassBV) {
					if(posizione.getGranularita() == LargeClassAgent.getGranularita()) {
						OccorrenzaSmell occorrenzaSmell = largeClassAgent.getJob().get();
						if(occorrenzaSmell.getName() != null) {
							occorrenzaSmells.add(occorrenzaSmell);
							System.out.println("LargeClassAgent aggiunge "+occorrenzaSmell);
						}
					}
				}
				
				if (longParListBV) {
					if(posizione.getGranularita() == LongParameterListAgent.getGranularita()) {
						OccorrenzaSmell occorrenzaSmell = longParameterListAgent.getJob().get();
						if(occorrenzaSmell.getName() != null) {
							occorrenzaSmells.add(occorrenzaSmell);
							System.out.println("LongParameterListAgent aggiunge "+occorrenzaSmell);
						}
					}
				}
				
				if (longMethodBV) {
					if(posizione.getGranularita() == LongParameterListAgent.getGranularita()) {
						OccorrenzaSmell occorrenzaSmell = longMethodAgent.getJob().get();
						if(occorrenzaSmell.getName() != null) {
							occorrenzaSmells.add(occorrenzaSmell);
							System.out.println("LongParameterListAgent aggiunge "+occorrenzaSmell);
						}
					}
				}
			}
		}
		
		pool.shutdown(); // Disable new tasks from being submitted
		try {
			// Wait a while for existing tasks to terminate
			if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
				pool.shutdownNow(); // Cancel currently executing tasks
				// Wait a while for tasks to respond to being cancelled
				if (!pool.awaitTermination(60, TimeUnit.SECONDS))
					System.err.println("Pool did not terminate");
			}
		} catch (InterruptedException ie) {
			// (Re-)Cancel if current thread also interrupted
			pool.shutdownNow();
			// Preserve interrupt status
			Thread.currentThread().interrupt();
		}
		
		calcolatoreMetriche.shutdown();
	}
	
	private void longMethodManyPar(Set<Granularita> GranularitaSet, boolean par1, boolean par2, boolean par3) {
		for (Entry<File, CompilationUnit> unit : project.getCompilationUnits().entrySet()) {
			Iterator<Granularita> granIterator = GranularitaSet.iterator();
			
			while(granIterator.hasNext()) {
				Granularita gran = granIterator.next();
				
				if(gran == Granularita.CLASS) {
					ClassVisitor visitor = new ClassVisitor();
					unit.getValue().accept(visitor);

					for (TypeDeclaration node : visitor.getClasses()) {
						Posizione posizione = new Posizione(node, Granularita.CLASS);
						posizioni.add(posizione);
					}
				}
				
				if(gran == Granularita.METHOD) {
					MethodVisitor visitor = new MethodVisitor();
					unit.getValue().accept(visitor);

					for (MethodDeclaration node : visitor.getMethods()) {
						Posizione posizione = new Posizione(node, Granularita.METHOD);
						posizioni.add(posizione);
					}
				}
			}
		}
	}
}
