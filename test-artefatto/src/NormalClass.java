
import java.util.ArrayList;
import java.util.List;

import jmav.object.OccorrenzaSmell;

public class NormalClass {
	private String name;
	private List<OccorrenzaSmell> childrenYes;
	private List<OccorrenzaSmell> children;
	
	public NormalClass(String name) {
		this.name = name;
		this.children = new ArrayList<OccorrenzaSmell>();
		this.childrenYes = new ArrayList<OccorrenzaSmell>();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void addChild(OccorrenzaSmell child) {
		children.add(child);
		
		if(child.isYes()) {
			childrenYes.add(child);
		}
	}
	
	public List<OccorrenzaSmell> getChildren() {
		return childrenYes;
	}
	
	public List<OccorrenzaSmell> getAllChildren() {
		return children;
	}
	
	public String toString() {
		return "{"+name + ": "+children+"}";
	}
}