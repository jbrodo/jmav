\subsubsection{Contesto applicativo}
La piattaforma Eclipse è costituita da una serie di sottosistemi costruiti al di sopra di un runtime engine. Questi sottosistemi prendono la forma di \textbf{plug-in}. Un plug-in è un modulo strutturato di codice e/o dati che aggiunge funzionalità al sistema. Tale contributo può essere fornito sotto forma di librerie di codice (classi Java con API pubblica), \textbf{estensioni} alla piattaforma o documentazione. Questa tecnica di estensione della piattaforma le conferisce una \textbf{struttura fortemente modulare} \ref{fig:sysview}.

\begin{figure} [H]
\centering
\includegraphics[scale=0.3]{../dias/EclipseArch2.png}
\caption{La struttura modulare di Eclipse. Nuovi plug-in estendono la piattaforma sfruttando il meccanismo degli \textit{Extension Points} (E.P.)}
\label{fig:sysview}
\end{figure}

Una regola di base per la costruzione di sistemi software modulari consiste nell'evitare uno stretto accoppiamento tra le componenti. In caso contrario diventa difficile assemblare tali componenti in diverse configurazioni o sostituire un componente senza causare un
profondo cambiamento nel sistema. Questo tipo di accoppiamento lasco in Eclipse si ottiene principalmente attraverso 
il meccanismo delle estensioni e dei \textbf{punti di estensione}. La metafora più
semplice per descrivere i punti di estensione è quella delle prese elettriche. Il singolo punto di estensione è rappresentato dalla presa, mentre l'estensione trova la sua controparte nella spina. Come per le prese elettriche, esistono diverse tipologie di punti di estensione, e solo le estensioni
che sono stati progettate per quel particolare punto di estensione si adattano.

\begin{figure} [H]
\centering
\includegraphics[scale=0.3]{../dias/jMAVinEclipse.png}
\caption{Interazione tra jMAV e le altre componenti della piattaforma Eclipse. L'utente interagisce indirettamente con diverse componenti del sistema.}
\label{fig:jMAVecl}
\end{figure}

All'interno di questo contesto si inserisce jMAV: esso è un plug-in di Eclipse, in particolare una estensione della piattaforma. Ogni plug-in di Eclipse può estendere ed interagire con qualsiasi altra componente: jMAV non fa eccezione, ed interagisce con la piattaforma esattamente come farebbe un qualsiasi altro plug-in (si veda la figura \ref{fig:jMAVecl}).

L'utente programmatore dunque interagisce direttamente con jMAV tramite l'interfaccia grafica che questo gli fornisce, ma contemporaneamente utilizza indirettamente altre componenti di Eclipse che il plug-in estende e sfrutta.


\begin{comment}
La figura \ref{fig:sistemview} mostra come l'utente interagisce con il sistema
jMAV. In particolare jMAV sar\`a un plugin di Eclipse [riferimento al
concetto?].

La piattaforma Eclipse è strutturata attorno al concetto di plug-in. I plug-in
sono fasci strutturati di codice e / o dati che contribuiscono con funzionalità al
sistema. La funzionalità può essere fornita in forma di librerie di codice
(classi Java con API pubbliche), estensioni della piattaforma, o anche
documentazione. 

Una regola di base per la costruzione di sistemi software modulari è di evitare
stretto accoppiamento tra le componenti. Se i componenti sono strettamente
integrati, diventa difficile assemblare i pezzi in diverse configurazioni o
sostituire un componente con una diversa implementazione senza causare un
repentino cambiamento in tutto il sistema.

Questo tipo di accoppiamento lasco in Eclipse si ottiene in parte attraverso 
il meccanismo delle estensioni e dei punti di estensione. La metafora più
semplice per descrivere i punti di estensione sono le prese elettriche. La presa, o
la presa, è il punto di estensione, la spina, o lampadina che si collega ad
esso, l'estensione. Come con le prese elettriche, i punti di estensione sono
disponibili in una grande varietà di forme e dimensioni, e solo le estensioni
che sono stati progettate per quel particolare punto di estensione si adattano.

Ogni plugin di Eclipse pu\`o interagire con qualsiasi altro plugin o componente,
che esso sia una funzionalit\`a, un frammento o un prodotto.

I Plugin si basano su bundle OSGi [CITAZIONE]. OSGi è utilizzato per gestire i plug-in
in un'applicazione Eclipse. Un plug-in deve contenere un file manifesto con
intestazioni OSGi validi per nome e la versione. Estensioni e
funzionalità e punti di estensione aggiunti da Eclipse sono gestiti da OSGi. Per
utilizzare le estensioni è necessario fornire un file plugin.xml. PDE \cite{PDE} fornisce
un progetto di piena funzionalità e editor per la creazione e la modifica di
questi file. L'implementazione OSGI utilizzata da Eclipse \`e chiamata Equinox
\cite{Equinox}.

Quindi l'utente si trover\`a ad utilizzare non solo jMAV ma anche altre componenti
dell'ecosistema creato da Eclipse.

\begin{figure}
\centering
\includegraphics[scale=0.4]{../dias/vista-del-sistema.png}
\caption{Vista del Sistema}
\label{fig:sistemview}
\end{figure}


Ogni sottosistema o componente nella piattaforma è strutturato come un insieme di
plug-in che implementa qualche funzione chiave. Alcuni plug-in aggiungono
caratteristiche visibili alla piattaforma utilizzando i punti di estensione.
Altri forniscono librerie di classi che possono essere utilizzati per
implementare estensioni per il sistema.

L'Eclipse SDK include la piattaforma di base più due grandi strumenti che sono
utili per i plug-in di sviluppo. Lo sviluppo di strumenti di Java (JDT)
implementa un ambiente di sviluppo completo per le funzionalità Java. Il plug-in
Developer Environment (PDE) aggiunge strumenti specializzati che semplificano lo
sviluppo di plug-in ed estensioni.

Questi strumenti non servono solo ad uno scopo utile, ma forniscono anche un
ottimo esempio di come i nuovi strumenti possono essere aggiunti alla
piattaforma come la costruzione di plug-in che estendono il sistema.

\begin{figure}
\centering
\includegraphics[scale=0.4]{../resources/ecplise-platform-arch.jpg}
\caption{Architettura della piattaforma di Eclipse}
\label{fig:sistemview}
\end{figure}

La componente Workbench contiene i punti di estensione che, per esempio,
permettono il plug-in per estendere l'interfaccia utente di Eclipse con
selezioni di menu e pulsanti della barra degli strumenti, per richiedere la
notifica di diversi tipi di eventi, e per creare nuove viste. La
componente Workspace contiene punti di estensione che consentono di interagire
con le risorse, compresi i progetti e file.

Il Workbench e il Workspace non sono gli unici componenti Eclipse che possono
essere estesi da altri plug-in, naturalmente. Inoltre, c'è una componente di
Debug che consente il tuo plug-in di lanciare un programma, interagire con il
programma in esecuzione, e gestire gli errori - tutto il necessario per
costruire un debugger. Vi è anche un componente della squadra che permette risorse Eclipse per
interagire con i sistemi di controllo di versione.

Infine, vi è una componente di Help a disposizione per consentire di fornire la
documentazione on-line e guida sensibili al contesto per l'applicazione. La documentazione è una parte essenziale di una
applicazione professionale, ma non è essenziale per le funzionalit\`a di un plug-in.

Nel nostro caso jMAV avr\`a la funzionalit\`a di interagire con la vista ad
albero comune a molti plugin di Eclipse tramite la componente WorkBench.
Tramite la vista ad albero permetter\`a di scegliere su quali file o cartelle
vogliamo eseguire l'analisi di code smell permettendo l'interazione tramite il
men\`u contestuale.
In fase di analisi interagiremo con la componente Workspace, nel dettaglio con
l'Abstract Syntax Tree.
Per decidere quali code smell jMAV deve cercare, come per ogni plugin, avr\`a un
pannello nella Gestione dei Preferiti.
Infine per poter visualizzare i risultati dell'analisi, jMAV permetter\`a
l'apertura tramite vista e l'esportazione dei risultati.
\end{comment}