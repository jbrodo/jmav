\subsubsection{Posizionamento del prodotto}
jMAV non vuole essere presentato come strumento dalle funzionalità innovative, ma piuttosto incorporare una serie di caratteristiche presenti in forma sparsa all'interno altri tool e riproporle in forma migliorata. Come già accennato in precedenza, sono numerosi gli strumenti per l'analisi statica di codice reperibili in rete. Questa sezione del documento riporta un elenco delle caratteristiche che un buon tool di analisi statica deve presentare. In seguito viene proposto un confronto tra alcuni tool che offrono funzionalità analoghe a quelle di jMAV e jMAV stesso, così che per il lettore sia più semplice posizionare il prodotto all'interno dell'ambito in cui si colloca.

\paragraph{Le caratteristiche desiderate}

Per poter meglio comprendere il confronto che ci si appresta ad effettuare bisogna chiarire quali siano i \textbf{termini di paragone} che possono essere utilizzati per stabilire se un tool di analisi statica sia valido o meno. In questo frangente, l'esperienza sviluppata dagli autori del documento in ambito di analisi di codice Java può essere d'aiuto.

Ogni code smell può essere individuato grazie all'utilizzo di una determinata \textbf{regola di detection}. Molte di queste regole utilizzano un certo numero di \textbf{metriche} (per esempio il numero di linee di codice o di metodi all'interno di una classe) per stabilire se un elemento del sistema è affetto o meno dallo smell: se il valore di una o più metriche calcolate sull'elemento analizzato supera una soglia prestabilita, allora quell'elemento è affetto dallo smell relativo alla regola in questione. Il valore delle soglie risulta dunque determinante ai fini dell'individuazione di uno smell.

Le definizioni di code smell utilizzate in letteratura nella maggior parte dei casi risultano piuttosto \textbf{ambigue}, lasciando spazio ad certo margine di interpretazione. Proprio per questo motivo è buona cosa lasciar decidere allo sviluppatore che si appresta ad effettuare un'analisi del codice quale debba essere il valore delle soglie da utilizzare all'interno delle regole di detection, che diventano dunque parzialmente \textbf{personalizzabili}.

Un altro aspetto fondamentale in un tool di analisi di codice Java riguarda la modalità di visualizzazione dei risultati dell'analisi: questa deve essere strutturata in maniera tale da risultare facilmente comprensibile e deve offrire una visione globale della porzione di codice analizzata che permetta all'utente di farsi un'idea più o meno immediata del livello di qualità dell'elemento del sistema studiato. In questo caso l'utilizzo di viste grafiche può rivelarsi molto utile.

Un ultimo aspetto importantissimo in un buon strumento di analisi riguarda la generazione dei report contenenti i risultati: questi devono essere esportabili e comprensibili. Inoltre risulta utilissimo avere a disposizione un report che consente di importare con facilità i risultati all'interno di un foglio di calcolo, che si è rivelato, nell'esperienza degli autori, di gran lunga lo strumento più comodo per effettuare lo studio dei risultati ottenuti dall'analisi, permettendo di generare in automatico un'ampia gamma di statistiche e grafici molto utili per valutare la qualità del sistema analizzato.

\paragraph{Confronto con tool analoghi}

Tra i molti tool reperibili in rete, per questo confronto sono stati scelti strumenti disponibili sotto forma di plug-in di Eclipse che presentano, nel loro insieme, diverse caratteristiche analoghe a quelle desiderate per jMAV. Uno degli scopi di questa sezione è infatti mettere in evidenza la natura eclettica del prodotto, che assimila le caratteristiche migliori di diversi strumenti di analisi statica del codice. I tool che sono stati presi in considerazione sono riportati in tabella \ref{tab:toollink}.

\begin{table}[H]
\centering
\begin{tabular}{rl}
\toprule
	Tool 	& Url Site \\
\midrule
Checkstyle 	& \url{http://checkstyle.sourceforge.net/} \\
\hline
inCode 		& \url{http://www.intooitus.com/products/incode} \\
\hline
JDeodorant 	& \url{http://www.jdeodorant.com/} \\
\hline
PMD 		& \url{http://pmd.sourceforge.net/} \\    
\bottomrule
\end{tabular}  
\caption{La lista dei tool con i rispettivi link}
\label{tab:toollink}
\end{table}


\begin{itemize}
\item{\textbf{Checkstyle}} \`{E} l'unico tool tra quelli elencati a non consentire l'estrazione automatizzata dei dati raccolti. Mostra riferimenti al codice visualizzabili all'interno dell'editor di testo di Eclipse. Ogni notifica di rilevamento smell viene mostrata nell'area dedicata alla visualizzazione delle notifiche di errore di compilazione e avvisi di warning di Eclipse. Permette sia di selezionare quali smell individuare che di modificare le soglie che caratterizzano le regole di detection.


\item{\textbf{inCode}} L'unico software a pagamento della rassegna.  Fornisce una vista grafica, chiamata dagli autori \textit{Diagram Map}, che permette di ottenere una visione d'insieme della qualità del progetto del quale si sta effettuando l'analisi. La \textit{Diagram Map} è un diagramma che raccoglie le classi per package di appartenenza e le colora in maniera differente a seconda della qualità del codice: si va dal rosso per indicare pessima qualità fino al bianco che contraddistingue codice di qualità elevata. \`{E} possibile esportare i risultati ma in un formato non adatto alla manipolazione tramite foglio di calcolo. La selezione delle regole, così come la modifica delle soglie, non è possibile.

 
\item{\textbf{JDeodorant}} Rileva quattro smell. Non è possibile cercarli contemporaneamente ma solo uno per volta. Per ogni smell è mostrata una tabella riassuntiva delle componenti del software che lo contengono e sono presenti riferimenti diretti al codice del progetto. Non è possibile modificare le soglie all'interno delle regole di detection. \`{E} possibile generare report esportabili ma i dati ottenuti non possono essere facilmente inseriti all'interno di un foglio di calcolo. JDeodorant permette di effettuare refactoring del codice automatizzato. 


\item{\textbf{PMD}} Questo tool permette di selezionare le regole da usare durante la ricerca ma non permette di modificarne le soglie. Fornisce riferimenti diretti al codice sorgente. Esporta i dati in modo che siano facilmente manipolabili tramite foglio di calcolo.
\end{itemize}

\paragraph{Sintesi delle caratteristiche}

Di seguito sono riportate alcune che sintetizzano i risultati del confronto effettuato. Vengono fornite tre tabelle: la tabella \ref{tab:toolsmell} indica gli smell identificati da ogni tool, la tabella \ref{tab:toolreport} mostra le caratteristiche dei report generati e la tabella \ref{tab:toolregole} evidenzia le possibilità di personalizzazione delle regole.
\begin{table}[H]
\centering
\begin{tabular}{rccccc}
\toprule
     Smell 			& Checkstyle &     inCode & JDeodorant &       jMAV &        PMD \\     \midrule
Data Class 			&          x &          x &            &          x &            \\
Data Clumps 		&            &          x &            &          x &            \\
Duplicated Code 	&          x &          x &            &          x &          x \\
Feature Envy 		&            &          x &          x &          x &            \\
Large Class 		&          x &          x &          x &          x &          x \\
Long Method 		&          x &          x &          x &          x &          x \\
Long Parameter List &          x &            &            &          x &          x \\
Message Chains 		&            &          x &            &          x &            \\
Refused Bequest 	&            &          x &            &          x &            \\
Shotgun Surgery 	&            &          x &            &          x &            \\
Type Checking 		&          x &            &          x &            &            \\
\bottomrule
\end{tabular}  
\caption{Smell riconosciuti dai tool}
\label{tab:toolsmell}
\end{table}


\begin{table}[H]
\centering
\begin{tabular}{rcccc}
\toprule
			&				&Report 		&				& \\
			&				&utilizzabile in &				& finestra \\
           	& Report 		&    foglio di  & Riferimenti  	& alternativa \\
     Tool  	& esportabile 	& calcolo		& al codice 	& a listato\\
\midrule
Checkstyle 	&            	&            	&          x 	&            \\
\hline
inCode 		&          x 	&       	 	&            	& x (package map) \\
\hline
JDeodorant 	&          x 	&        	 	&            	& x (tabella) \\
\hline
jMAV 		& 		x		&	x			&		x		&		\\
\hline
PMD 		&          x 	& x				&			x 	&	 \\
\bottomrule
\end{tabular}
\caption{Caratteristiche dei report dei tool}
\label{tab:toolreport}
\end{table}

\begin{table}[H]
\centering
\begin{tabular}{rcc}
\toprule
           		& Personalizzare & Creare \\
           		& regole 			& regole\\
\midrule   
Checkstyle	 	& x 				&	x\\
\hline  
inCode	 		&     				&       \\
\hline
jDeodorant 		&      				&      \\ 
\hline
jMAV			& x 				& 	\\
\hline
PMD 			&    				&       \\
\bottomrule
\end{tabular}  
\caption{Personalizzazione regole di detection}
\label{tab:toolregole}
\end{table}

In conclusione, ogni tool tra quelli analizzati mostra punti di vantaggio e svantaggio. La scelta di tool che permettono di modificare le soglie delle regole è molto ridotta: tra gli strumenti riportati solo Checkstyle consente la personalizzazione delle soglie. Inoltre, solamente PMD consente la generazione dei report in formato tale da permettere il facile inserimento dei risultati ottenuti all'interno di un foglio di calcolo.
%L'obiettivo di jMAV risulta a questo punto semplice da delineare: fornire agli sviluppatori uno strumento di analisi statica del codice java che integri tutte le migliori caratteristiche dei tool già presenti in rete, semplificando così il lavoro degli sviluppatori. 

%Tutti i tool presi in considerazione permettono di analizzare codice java automaticamente.  inCode è il tool a pagamento della rassegna. I tool PMD e Checkstyle permettono di selezionare l'insieme di regole di detection che si vogliono applicare come mostrato in tabella \ref{tab:toolregole}. Checkstyle è l'unico tool che permette di modificare le soglie delle regole di detection. JDeodorant invece permette di effettuare la ricerca di un solo smell per volta.
%
%I report generati dai tool di questa rassegna sono variegati. Alcuni possono essere salvati altri no. jMav è stato pensato per esportare in formati comodi per essere elaborati facilmente tramite fogli di calcolo (ad esempio csv o tsv). PMD e Checkstyle mostrano i loro risultati sfruttando i warning e le notifiche di errore predisposte da Eclipse, che questultimo solitamente usa per notificare errori di compilazione e di configurazione dei progetti. Inoltre usano la notifica a lato del codice sorgente, permettendo riferimenti al codice immediati. inCode si differenzia perché ha una vista chiamata PackageMap per mostrare lo stato delle componenti divise in package. Jdeodorant mostra semplicemente una tabella, per tipologia di smell, con il listato delle componenti che ne. PMD, inCode e JDeodorant permettono di salvare il report in formato testuale.
%
%Nessuno di questi tool permette di esportare in formati utili per l'utilizzo tramite foglio di calcolo a parte PMD. La tabella \ref{tab:toolreport} mette in evidenza che solo PMD esporta in in CSV facilmente importabile con nun foglio di calcolo e che Checkstyle non permette di estrarre i dati delle analisi.



